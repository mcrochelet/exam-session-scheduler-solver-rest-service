#!/bin/bash 

if [ "$#" -ne 1 ] || ! [ -d "$1" ]; then
  echo "Usage: $0 OSCAR_DIRECTORY" >&2
  exit 1
fi

cd $1;
sbt publish-local "project oscar-algo" publish-local "project oscar-util" publish-local |grep "::";
cd -;

sbt clean update;