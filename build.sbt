name := "Exam Session Scheduler"

version := "1.0.0"

scalaVersion := "2.11.6"

mainClass in Compile := Some("api.Boot")

libraryDependencies ++= {
  Seq(
     "oscar"                   %  "oscar-cp_2.11"   % "latest.integration"
    ,"junit"                   %  "junit"           % "latest.integration" % "test"
    ,"io.spray"                %% "spray-can"       % "latest.integration" withSources() withJavadoc()
    ,"io.spray"                %% "spray-routing"   % "latest.integration" withSources() withJavadoc()
    ,"io.spray"                %% "spray-json"      % "latest.integration"
    ,"io.spray"                %% "spray-client"    % "latest.integration"
    ,"io.spray"                %% "spray-testkit"   % "latest.integration" % "test"
    ,"com.typesafe.akka"       %% "akka-actor"      % "latest.integration"
    ,"com.typesafe.akka"       %% "akka-testkit"    % "latest.integration" % "test"
    ,"org.scala-lang.modules"  %% "scala-xml"       % "1.0.3" withSources() withJavadoc()
    ,"org.scala-lang"          % "scala-reflect"    % "2.11.6"
  )
}
