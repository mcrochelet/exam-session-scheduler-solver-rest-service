package test.solver

import launcher.util.ESSScaffolder
import org.junit.Assert._
import org.junit.Test

import scala.collection.mutable._
import solver._

class ESSModelTest {

  var parsedData: ESSScaffolder = new ESSScaffolder(24, 0, 2, "resources/courses_students.csv", Option("resources/multislot_oral_exams.txt"), Option("resources/session_restriction.txt"))
  val model = new ESSModel(parsedData) with SearchLauncher

  @Test
  def studentArraySize {
    assertEquals("Student Array size should be ", parsedData.students.size, model.studentSchedule.keys.size)
  }

  @Test
  def filtering {
    val rSession = (0 until parsedData.nSession()).filter(model.isPossibleSlotForBeginningOfMultislotExamOf(3, _))
    assertTrue("rSession should be empty", Vector().equals(rSession))

  }

  @Test
  def checkDecisionVariablesNumber {
    assertEquals("Number of decision variables shoud be ", model.unislotExams.size
      + model.multislotsExams.toArray.map { x => parsedData.numberOfSlotsFor(x) }.sum
      + model.oralExams.toArray.map { x => parsedData.numberOfSlotsForOral(x) + parsedData.studentFollowingCourse(x).size }.sum, model.decisionVariables.size)
  }

  @Test
  def checkSolutionFound {
    var solFound = false
    val solutionStatistics = model.startSearch(10000, (solution: Map[String, Array[Int]], stat: scala.collection.Seq[(Int, Int)]) => {}, () => { solFound = true })
    assertTrue("At least one solution should have been found ", solFound)
  }

  /**
   * ********************************************
   *      Following tests solution validity
   * ********************************************
   */

  /**
   * No oral session in same time than other course session of a professor
   */
  @Test
  def fullAvailabilityForOral {
    val solutionStatistics = model.startSearch(10000, (solution: Map[String, Array[Int]], stat: scala.collection.Seq[(Int, Int)]) => {
      for (oral <- model.oralExams) {
        for (profId <- parsedData.professorsGivingCourse(oral)) {

          val valueArray = parsedData.oralsGivenByProfessor(profId).map(model.getCPIntOfCourse(_).toArray).flatten.map(_.value)

          for (oralSession <- model.oralsSchedule(oral)) {
            val assertName = "Professor has no other exam in the same time than oral session " + oral
            assertTrue(assertName, valueArray.count(_ == oralSession.value) == 1)
          }
        }
      }
    }, () => {})
  }

  /**
   *  Number of students in the same slot do not overload maximum capacity
   */
  @Test
  def noOverloadOralCapacity {
    val solutionStatistics = model.startSearch(10000, (solution: Map[String, Array[Int]], stat: scala.collection.Seq[(Int, Int)]) => {
      for (oral <- model.oralExams) {
        val allStudents = parsedData.studentFollowingCourse(oral).toArray
        for (oralSession <- model.oralsSchedule(oral)) {
          val nStudentInSlot = allStudents.map(model.studentSchedule(_)(oral)(0).value).count(_ == oralSession.value)
          assertTrue("Do not overload student capacity", nStudentInSlot <= parsedData.nStudentPerOralSession(oral))
        }
      }
    }, () => {})
  }

}
