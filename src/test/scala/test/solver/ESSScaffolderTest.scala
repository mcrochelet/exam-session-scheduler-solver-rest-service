package test.solver

import launcher.util.ESSScaffolder
import org.junit.Assert._
import org.junit.Test

class ESSScaffolderTest {
  val nDays = 24
  val sessionPerDay = 2
  var parsedData: ESSScaffolder = new ESSScaffolder(nDays,0, sessionPerDay, "")

  @Test
  def fileParsing {
    parsedData.readCourseFile("resources/courses_students.csv")
    assertEquals("Arrays should have the same size", parsedData.coursesFollowedBy.keys.size, parsedData.students.size)
    assertEquals("Arrays should have the same size", parsedData.professorsGivingCourse.keys.size, parsedData.courses.size)
  }
}
