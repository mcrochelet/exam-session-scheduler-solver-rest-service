package test.api.model

import org.junit.Assert._
import org.junit._
import akka.actor._
import akka.pattern._
import api.model._
import api.model.EncapsulatedModelProtocol._
import scala.concurrent.duration.DurationInt
import akka.util._

class EncapsulatedModelTest {

  implicit val system = ActorSystem("ess-server")
  val model = system.actorOf(Props[EncapsulatedModel], "Testing-Actor")

  implicit val timeout = Timeout(5.seconds) // needed for `?` below
  implicit val ec = system.dispatcher
  /* TODO
    @Test
    def checkNumberOfDays {
      model ! setNumberOfDays(42)
      val modelNumberOfDays = (model ? getNumberOfDays)
        .mapTo[Int]
        .onSuccess({
          case n =>
            assertEquals("The number of days in the model should return ", 42, n)
        })
    }
    @Test
    def checkNumberOfSessionPerDays {
      model ! setNumberOfSessionPerDay(2)
      val modelNumberOfSessionPerDays = (model ? getNumberOfSessionPerDay)
        .mapTo[Int]
        .onSuccess({
          case n =>
            assertEquals("The number of session per days in the model should return ", 2, n)
        })
    }

    @Test
    def addStudentTest {
      model ! addStudent("Martin Crochelet")
      val modelStudents = (model ? getStudents)
        .mapTo[Set[String]]
        .onSuccess({
          case students =>
            assertTrue("The students array should contain ", students.contains("Martin Crochelet"))
        })
    }
    @Test
    def removeStudentTest {
      val studentsBefore = (model ? getStudents)
        .mapTo[Set[String]]
        .map({
          case before => {
            model ! addStudent("Martin Crochelet")
            model ! removeStudent("Martin Crochelet")
            val studentsAfter = (model ? GetStudents)
              .mapTo[Set[String]]
              .onSuccess({
                case after =>
                  assertTrue("The differential array should be of size ", before.diff(after).size == 0)
              })
          }
        })
    }

    @Test
    def addCourseTest {
      model ! addCourse("LFSAB1001")
      val modelCourses = (model ? getCourses)
        .mapTo[Set[String]]
        .onSuccess({
          case courses =>
            assertTrue("The students array should contain ", courses.contains("Martin Crochelet"))
        })
    }
    @Test
    def removeCourseTest {
      val coursesBefore = (model ? getCourses)
        .mapTo[Set[String]]
        .map({
          case before => {
            model ! addCourse("LFSAB1001")
            model ! removeCourse("LFSAB1001")
            val studentsAfter = (model ? GetCourses)
              .mapTo[Set[String]]
              .onSuccess({
                case after =>
                  assertTrue("The differential array should be of size ", before.diff(after).size == 0)
              })
          }
        })
    }

    @Test
    def addProfessorTest {
      model ! addProfessor("Pierre Schaus")
      val modelProfessors = (model ? getProfessors)
        .mapTo[Set[String]]
        .onSuccess({
          case professors =>
            assertTrue("The students array should contain ", professors.contains("Martin Crochelet"))
        })
    }
    @Test
    def removeProfessorTest {
      val professorsBefore = (model ? getProfessors)
        .mapTo[Set[String]]
        .map({
          case before => {
            model ! addProfessor("Pierre Schaus")
            model ! removeProfessor("Pierre Schaus")
            val professorsAfter = (model ? GetProfessors)
              .mapTo[Set[String]]
              .onSuccess({
                case after =>
                  assertTrue("The differential array should be of size ", before.diff(after).size == 0)
              })
          }
        })
    }
  */
  // TODO addStudentFollowingCourse(student: String, course: String)
  // TODO addStudentFollowingCourses(student: String, course: Set[String])
  // TODO getStudentsFollowingCourse(course: String)
  // TODO getCoursesFollowedByStudent(student: String)
  // TODO addProfessorGivingCourse(prof: String, course: String)
  // TODO addProfessorGivingCourses(prof: String, courses: Set[String])
  // TODO addProfessorsGivingCourse(profs: Set[String], course: String)
  // TODO addESSConstraint(constraintType: String, args: List[String])
  // TODO search(onSolution: () => Unit)
  // TODO PrintInstanceMetrics

}
