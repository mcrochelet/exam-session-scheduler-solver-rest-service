package test.util

import org.junit._
import org.junit.Assert._

import scala.collection.mutable._
import util._
/**
 * Created by mcrochelet on 9/05/15.
 */
class MapWithTest {
  val mapIntString = Map[Int, String] (
    1 -> "a",
    2 -> "b",
    3 -> "a"
  )
  val mapIntInt    = Map[Int, Int] (
    1 -> 1,
    2 -> 1,
    3 -> 1
  )

  val emptyMap     = Map[Int, Int] ()

  @Test
  def MapWithContainsValueTest {
    assertTrue("mapIntString.containsValue('a') should return true ", mapIntString.containsValue("a"))
    assertTrue("mapIntString.containsValue('a') should return false ", !emptyMap.containsValue(1))
    assertTrue("mapIntInt.containsValue(1) should return true ", mapIntInt.containsValue(1))
  }

  @Test
  def MapWithKeysOfTest {
    assertTrue("mapIntString.keysOf('a') should return ", mapIntString.keysOf("a").diff(Array[Int](1, 3)).size == 0)
    assertTrue("mapIntString.keysOf('a') should return ", emptyMap.keysOf(1).diff(Array[Int]()).size == 0)
    assertTrue("mapIntInt.keysOf(1) should return ", mapIntInt.keysOf(1).diff(Array[Int](1, 2, 3)).size == 0)
  }

}
