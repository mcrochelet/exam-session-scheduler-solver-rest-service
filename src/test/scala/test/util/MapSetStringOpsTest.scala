package test.util

import org.junit.Assert._
import org.junit._
import util._

import scala.collection.mutable._
/**
 * @author mCrochelet
 */
class MapSetStringOpsTest {

  val mapSet = Map[String, Set[String]]()

  @Test
  def testAddition {
    mapSet += "k" -> "v"
    assertTrue("mapSet should contain key 'k'", "v".equals(mapSet.get("k").orNull.toSeq(0)))
    mapSet.clear()
  }

  @Test
  def testIterator {
    mapSet += "k0" -> "v0"
    mapSet += "k0" -> "v1"
    mapSet += "k1" -> "v1"
    mapSet += "k1" -> "v1"
    mapSet += "k2" -> "v2"

    assertTrue("k2 should contain only v2", Set[String]("v2").equals(mapSet.get("k2").orNull))
    assertTrue("k1 should contain only v1", Set[String]("v1").equals(mapSet.get("k1").orNull))
    assertTrue("k0 should contain v0, v1", Set[String]("v0", "v1").equals(mapSet.get("k0").orNull))
    mapSet.clear()
  }

  @Test
  def testSuppression {
    mapSet += "k" -> "v"
    mapSet -= "k" -> "v"
    assertTrue("mapSet should be empty set", Set[String]().equals(mapSet.get("k").orNull))
    mapSet.clear()
  }
}
