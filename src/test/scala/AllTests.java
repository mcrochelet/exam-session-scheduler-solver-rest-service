import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.solver.*;
import test.util.*;
import test.api.model.*;

@RunWith(Suite.class)
@SuiteClasses({ ESSScaffolderTest.class, ESSModelTest.class, MapWithTest.class, MapSetStringOpsTest.class, EncapsulatedModelTest.class})
class AllTests {

} 
 