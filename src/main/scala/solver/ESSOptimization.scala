package solver
import oscar.cp._
import scala.math._

/* Weighted Sum optimization */
trait OptimizationFunctionWS extends OptimizationGMDG {
  // weighted sum
  maximize(objective1toMax * UBobjective2 - objective2)

  var bestObjective1 = objective1.max
  var bestObjective2 = objective2.max

  onSolution {
    bestObjective1 = min(objective1.value, bestObjective1)
    bestObjective2 = min(objective2.value, bestObjective2)
  }
}

/* VO LNS equivalent to previous WS */
trait OptimizationFunctionVOLNS extends OptimizationGMDG {
  // Equivalent VO LNS
  solver.minimize(objective1, objective2)
  solver.obj(objective1).tightenMode = TightenType.StrongTighten
  solver.obj(objective2).tightenMode = TightenType.NoTighten

  var bestObjective1 = objective1.max
  var bestObjective2 = objective2.max

  onSolution {
    bestObjective1 = min(objective1.value, bestObjective1)
    bestObjective2 = min(objective2.value, bestObjective2)
  }
}

/* Official Optimization Function */
trait OptimizationFunction extends OptimizationGMDG with OpimizationOnClash with OptimizationDayDistribution /*with OptimizationForProfessors */ {

  // More complex VO LNS
  solver.minimize(objective4 :+ objective0 :+ objective1 :+ objective2: _*)
  solver.obj(objective0).tightenMode = TightenType.NoTighten
  solver.obj(objective1).tightenMode = TightenType.StrongTighten
  solver.obj(objective2).tightenMode = TightenType.NoTighten
  for (obj <- objective4)
    solver.obj(obj).tightenMode = TightenType.NoTighten

  var bestObjective0 = objective0.max
  var bestObjective1 = objective1.max
  var bestObjective2 = objective2.max
  var bestObjective4 = Array.fill(objective4.size)(objective4.last.max)

  onSolution {
    /* Solution will always be better */
    bestObjective0 = objective0.value
    bestObjective1 = objective1.value
    bestObjective2 = objective2.value
    for (i <- 0 until bestObjective4.size)
      bestObjective4(i) = objective4(i).value

  }

}



