package solver
import oscar.cp._
import scala.collection.mutable._
import scala.math._

/**
 * **************************************************************************
 *                        Optimization function
 * **************************************************************************
 */

/**
 * OpimizationOnClash
 *
 * Objective0 : To minimize
 * Represents the sum of violations for students
 *
 */
trait OpimizationOnClash extends ESSModel {
  // Define the upper bound of clashing
  val UBobjective0 = data.students.map(data.coursesFollowedBy(_).size - 1).sum

  // Declare the variable representing violations
  val objective0 = CPIntVar(0 to UBobjective0, name = "Objective 0")

  // Objective0 : contains the sum of clash for students
  add(objective0 === sum(studentViolation.values))
}

/**
 * OptimizationGMSG
 * Maximize the global minimal session gap (GMSG)
 * and the number of students having it as minimal gap
 *
 * objective1toMax : To maximize
 * The global minimal session gap
 *
 * objective1      : To minimize
 * The opposite of objective1toMax
 *
 * objective2      : To minimize
 * The number of student having GMSG as own minimal gap
 *
 */
trait OptimizationGMSG extends ESSModel {

  // Define all upper bounds
  val UBobjective1 = nSession
  val UBobjective2 = data.students.size

  // Define all objectives
  val objective1toMax = CPIntVar(0 until UBobjective1, name = "Objective 1")
  val objective2 = CPIntVar(0 to UBobjective2, name = "Objective 2")
  val objective1 = -objective1toMax

  // Objective1toMax : contains the global minimal gap
  add(minimum(minimalConsecutiveFreeSessionFor.values.toArray, objective1toMax))

  // Objective2 : contains the number of student having global gap as minimal gap
  add(countEq(objective2, minimalConsecutiveFreeSessionFor.values.toArray, objective1toMax))

}

/**
 * OptimizationGMDG
 * Maximize the global minimal day gap (GMDG)
 * and the number of students having it as minimal gap
 *
 * objective1toMax : to maximize
 * The global minimal day gap
 *
 * objective1      : to minimize
 * The opposite of objective1toMax
 *
 * objective2      : to minimize
 * The number of student having GMDG as own minimal gap
 *
 */
trait OptimizationGMDG extends ESSModel with LinkingDay {

  // maxEnrollement represents the maximum number of enrollment of a student
  val maxEnrollement = data.coursesFollowedBy.values.map(_.size).max

  /*
   * Tests is a specified mentioned day gap is theoretically reachable by
   * student having the maximum enrollment
   * 
   * @return  true    if the day gap could be reach
   *          false   otherwise
   */
  def isNotPossible(dayGap: Int): Boolean = {
    var tmpDay, counter = 0
    while (counter < maxEnrollement && tmpDay < data.nDays) {
      counter += 1
      tmpDay += dayGap

      // If on sunday, we set increment var by 1 to switch on monday
      if (tmpDay % 7 == 6)
        tmpDay += 1
    }
    return (counter < maxEnrollement)
  }

  // Allows us to create an infinite stream
  def streamFrom(from: Int): Stream[Int] = Stream.cons(from, streamFrom(from + 1))

  // Define all upper bounds
  val UBobjective1 = (streamFrom(1) filter isNotPossible)(0) - 1
  val UBobjective2 = data.students.size

  // Define all objectives
  val objective1toMax = CPIntVar(0 to UBobjective1, name = "Objective 1")
  val objective2 = CPIntVar(0 to UBobjective2, name = "Objective 2")
  val objective1 = -objective1toMax

  // Objective1toMax contains the global minimal day gap
  add(minimum(minimalConsecutiveFreeDaysFor.values.toArray, objective1toMax))

  // Objective2 contains the number of student having global gap as minimal gap
  add(countEq(objective2, minimalConsecutiveFreeDaysFor.values.toArray, objective1toMax))

}

/**
 * OptimizationForProfessors  -- Still Experimental --
 *
 * objective3 - to minimize
 * represent the sum of the number of student sitting in a session times this sessionID
 * for each sessionID.
 *
 * In order to have more students at the beginning, we try to minimize this value
 *
 */
trait OptimizationForProfessors extends ESSModel {

  // Define the upper bound
  val UBobjective3 = data.writtenGivenByProfessor.values.flatten.map(data.studentFollowingCourse(_).size).sum * nSession

  // Define the objective
  val objective3 = CPIntVar(0 until UBobjective3, name = "Objective 3")

  // Objective3 contains Weighted Sum.
  add(objective3 == sum(
    {
      val courses = data.writtenGivenByProfessor.values.flatten
      courses.map(c => (-getCPIntOfCourse(c)(0) + nSession - 1) * data.studentFollowingCourse(c).size)
    }))
}

/**
 * OptimizationDistribution
 *
 * objective4 - represent the number of student having each possible gap as minimal gap (to minimize on low gap)
 *
 */
trait OptimizationDistribution extends ESSModel {

  // Define all upper bounds
  val UBobjective4 = data.students.size

  // Define all objectives
  val objective4 = Array.fill(data.nDays)(CPIntVar(0 until UBobjective4, name = "Objective 4"))

  // Objective 4 contains the number of student having each gap as minimal gap
  for (i <- 0 until data.nDays)
    add(countEq(objective4(i), minimalConsecutiveFreeSessionFor.values.toIndexedSeq, i))
}

/**
 * OptimizationDayDistribution
 *
 * objective4 : to minimize on low gap (first cells of the array)
 * represent the number of student having each possible gap as minimal gap
 *
 */
trait OptimizationDayDistribution extends ESSModel with LinkingDay {

  // Define all upper bounds
  val UBobjective4 = data.students.size

  // Define all objectives
  val objective4 = Array.fill(data.nDays)(CPIntVar(0 until UBobjective4, name = "Objective 4"))

  // Objective 4 : Minimizing the number of student having low minimal gap
  for (i <- 0 until data.nDays)
    add(countEq(objective4(i), minimalConsecutiveFreeDaysFor.values.toIndexedSeq, i))
}

/**
 * OptimizationGapProduct  -- Still Experimental --
 *
 * Maximize comfort of students with function :
 *     sum[prod(gapsBetwwenCoursesOf(student))]
 *
 *   objective7 - To maximize
 *   stores the result of the previous weighted sum
 *
 */
trait OptimizationGapProduct extends ESSModel {

  // Define the upper bound
  val UBobjective7 = Int.MaxValue

  // Define the objective
  val objective7 = CPIntVar(0 until UBobjective7, name = "Objective 1")
  val minDistForCourseForStud: Map[String, Map[String, CPIntVar]] = Map()
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1); c1 <- data.coursesFollowedBy(student))
    minDistForCourseForStud += student -> Map(c1 -> CPIntVar(0 until nSession, name = "min for " + student + " for course " + c1))

  /**
   * Counting #student having each gap as minimal
   */
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1); c1 <- data.coursesFollowedBy(student)) {
    // Retrieve all minimal gap values for course c1
    val allGapValuesForCourse = for (c2 <- data.coursesFollowedBy(student); if (c1 != c2))
      yield Array(spaceBetweenCoursesOf(student)(c1, c2), spaceBetweenCoursesOf(student)(c2, c1))

    add(minimum(allGapValuesForCourse.toArray.flatten, minDistForCourseForStud(student)(c1)))
  }

  /**
   * Objective 7 : Minimizing the minimal gap of students with weighted sum
   */
  add(objective7 ===
    minDistForCourseForStud.values.map(student => student.values.foldLeft(CPIntVar(1))((acc, num) => (num * acc))).foldLeft(CPIntVar(0))((acc, num) => (num + acc)))
}
