package solver
import oscar.visual._
import oscar.visual.shapes._
import java.awt.Color._
import oscar.cp._

/* Visualization of barplot in terms of day gaps */
trait BarplotDays extends LinkingDay {

  val frame = VisualFrame("Distribution")

  val tour = VisualDrawing(true, false)
  frame.add(tour)

  val range = 0 until data.nDays
  val students = data.students.filter(student => data.coursesFollowedBy(student).size > 1)
  val largeur = 20
  val res = range.map(elem => new VisualRectangle(tour, largeur * elem, 10, largeur, 0))
  res.map(_.innerCol_=(LIGHT_GRAY))

  frame.pack()
  frame.setSize(700, 400)

  onSolution {
    val tmp = range.map(nMin => (nMin, students.filter(student => minimalConsecutiveFreeDaysFor(student).min == nMin).size))
    tmp.map(elem => res(elem._1).height_=(elem._2))
  }
}

/* Visualization of barplot in terms of session gaps */
trait BarplotSession extends ESSModel {

  val frame = VisualFrame("Distribution")

  val tour = VisualDrawing(true, false)
  frame.add(tour)

  val range = 0 until data.nDays
  val students = data.students.filter(student => data.coursesFollowedBy(student).size > 1)
  val largeur = 20
  val res = range.map(elem => new VisualRectangle(tour, largeur * elem, 10, largeur, 0))
  res.map(_.innerCol_=(LIGHT_GRAY))

  frame.pack()
  frame.setSize(700, 400)

  onSolution {
    val tmp = range.map(nMin => (nMin, students.filter(student => minimalConsecutiveFreeSessionFor(student).min == nMin).size))
    tmp.map(elem => res(elem._1).height_=(elem._2))
  }
}