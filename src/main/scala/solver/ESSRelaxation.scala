package solver
import oscar.cp._
import scala.collection.mutable._
import oscar.util._
import scala.util.Random
import scala.math._

/**
 * **************************************************************************
 *                        Search with relaxation
 * **************************************************************************
 */

/**
 * RandomRelaxation
 * Assign randomly 70% of variables from the last solution found
 */
trait RandomRelaxation extends ESSModel with OptimizationFunction {
  val startTime = java.lang.System.currentTimeMillis();
  def startSearch(maxTime: Int, callback: (Map[String, Array[Int]], scala.collection.Seq[(Int, Int)]) => Unit, fcb: () => Unit) = {
    cp.addDecisionVariables(decisionVariables)
    onSolutionCallback = callback

    /* 
     * Search for first solution avoiding long trail
     * the value of 1800 backtracks 
     * has been found experimentally
     */
    var solved = false
    while (!solved) {
      val stats = start(nSols = 1, failureLimit = 1800)
      solved = stats.nSols == 1
    }

    while (java.lang.System.currentTimeMillis() - startTime < maxTime) {
      /* Changing tightening mode of objectives */
      if (bestObjective1 == -UBobjective1) {
        solver.obj(objective1).tightenMode = TightenType.WeakTighten
        solver.obj(objective2).tightenMode = TightenType.StrongTighten
      }

      /* Relax the previous solution */
      lastSolutionMetrics = startSubjectTo(failureLimit = 1800) {
        for (i <- 0 until decisionVariables.size; if rand.nextInt(100) < 70)
          add(decisionVariables(i) == cp.lastSol(decisionVariables(i)))
      }
    }
    fcb()
    lastSolutionMetrics

  }
}

/**
 * RandomAdaptativeRelaxation
 * Randomized Adaptative Search --
 * Assign randomly between "neighbourhood" of variables from the last solution found
 */
trait RandomAdaptativeRelaxation extends ESSModel with OptimizationFunction {
  val startTime = java.lang.System.currentTimeMillis();

  def startSearch(maxTime: Int, callback: (Map[String, Array[Int]], scala.collection.Seq[(Int, Int)]) => Unit, fcb: () => Unit) = {
    cp.addDecisionVariables(decisionVariables)
    onSolutionCallback = callback

    /* 
     * Search for first solution avoiding long trail
     * the value of 2500 backtracks 
     * has been found experimentally
     */
    var solved = false
    val failureLimit = 2500
    while (!solved) {
      val stats = start(nSols = 1, failureLimit = failureLimit)
      solved = stats.nSols == 1
    }

    var environement = 70
    while (java.lang.System.currentTimeMillis() - startTime < maxTime) {
      lastSolutionMetrics = startSubjectTo(failureLimit = 1800) {
        /* Changing tightening mode of objectives */

        /*if (bestObjective0 == 0) {
          solver.obj(objective0).tightenMode = TightenType.WeakTighten
          solver.obj(objective1).tightenMode = TightenType.StrongTighten
        }*/
        /* If best obj1 found */
        if (bestObjective1 == -UBobjective1) {
          solver.obj(objective1).tightenMode = TightenType.WeakTighten
          solver.obj(objective2).tightenMode = TightenType.StrongTighten
        }

        /* Random Relaxation */
        for (i <- 0 until decisionVariables.size; if rand.nextInt(100) < environement)
          add(decisionVariables(i) == cp.lastSol(decisionVariables(i)))
      }

      /* If search stopped because all subtree has been visited,
       * we enlarge the neighborhood otherwise, we reduce it
       */
      if (lastSolutionMetrics.completed)
        environement = max(environement - 10, 0)
      else
        environement = max(environement - 1, 0)
      /* If found a solution, we set back the environment to 70 */
      if (lastSolutionMetrics.nSols >= 1)
        environement = 70
    }
    fcb()
    lastSolutionMetrics

  }
}

/**
 * WorstCourseAndRandomRelaxation
 * Assign randomly "neighbourhood"% of courses from the last solution found among those
 * whose minimal gap with another course is higher than the global one
 * (when both courses are followed by at least a student)
 *
 * Otherwise, we don't assign courses who cause the objective to stuck
 *
 */
trait WorstCourseAndRandomRelaxation extends LinkingDay with OptimizationFunction {
  val startTime = java.lang.System.currentTimeMillis();
  var timeLastSolution = startTime

  onSolution {
    timeLastSolution = java.lang.System.currentTimeMillis()
  }

  def startSearch(maxTime: Int, callback: (Map[String, Array[Int]], scala.collection.Seq[(Int, Int)]) => Unit, fcb: () => Unit) = {
    println(s"with max time: $maxTime")
    cp.addDecisionVariables(decisionVariables)

    onSolutionCallback = callback

    /* 
     * Search for first solution avoiding long trail
     * the value of 2500 backtracks 
     * has been found experimentally
     */
    var solved = false
    val failureLimit = 2500
    while (!solved) {
      val stats = start(nSols = 1, failureLimit = failureLimit)
      solved = stats.nSols == 1
    }

    var environement = 70
    while (java.lang.System.currentTimeMillis() - startTime < maxTime) {
      // Search 3 -- Relaxation on worst values on top of random relaxation
      lastSolutionMetrics = startSubjectTo(failureLimit = failureLimit) {
        /* Changing tightening mode of objectives */

        /* If we are stuck for too long in the value before the theoretical best */
        if (bestObjective1 == -UBobjective1 + 1 && (java.lang.System.currentTimeMillis() - timeLastSolution) > maxTime / 15) {
          if (rand.nextInt(100) < 50) {
            solver.obj(objective1).tightenMode = TightenType.WeakTighten
            solver.obj(objective2).tightenMode = TightenType.StrongTighten
          } else {
            solver.obj(objective1).tightenMode = TightenType.StrongTighten
            solver.obj(objective2).tightenMode = TightenType.NoTighten
          }
        }
        /* If best objective1 is found */
        if (bestObjective1 == -UBobjective1) {
          solver.obj(objective1).tightenMode = TightenType.WeakTighten
          solver.obj(objective2).tightenMode = TightenType.StrongTighten

          /* When objective2 has been optimized enough, we sometimes switch to obj4 */
          if (bestObjective2 < UBobjective2 / 4) {
            if (rand.nextInt(100) < 50) {
              solver.obj(objective4(-bestObjective1 + 1)).tightenMode = TightenType.StrongTighten
              solver.obj(objective2).tightenMode = TightenType.WeakTighten
            } else {
              solver.obj(objective4(-bestObjective1 + 1)).tightenMode = TightenType.NoTighten
              solver.obj(objective2).tightenMode = TightenType.StrongTighten
            }
          }
        }

        /* Relaxation on worst values */
        for (c1 <- data.courses) {
          val CPCourses = for (student <- data.studentFollowingCourse(c1); c2 <- data.coursesFollowedBy(student); if c1 != c2) yield {
            if (c1 < c2)
              cp.lastSol(daysBetweenCoursesOf(student)(c1, c2)) > -bestObjective1
            else
              cp.lastSol(daysBetweenCoursesOf(student)(c2, c1)) > -bestObjective1
          }
          if (CPCourses.forall(x => x) && writtenExams.contains(c1) && rand.nextInt(100) < environement)
            add(writtenDaySchedule(c1) == cp.lastSol(writtenDaySchedule(c1)))
        }

      }
      /* If search stopped because all subtree has been visited,
       * we enlarge the neighborhood otherwise, we reduce it
       */
      if (lastSolutionMetrics.completed)
        environement = max(environement - 10, 0)
      else
        environement = max(environement - 1, 0)
      /* If found a solution, we set back the environment to 70 */
      if (lastSolutionMetrics.nSols >= 1)
        environement = max(environement + 2, 70)
    }
    fcb()
    lastSolutionMetrics

  }

}

