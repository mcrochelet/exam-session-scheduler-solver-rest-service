package solver
import oscar.cp._
import scala.collection.mutable._

trait LinkingDay extends ESSModel {

  /**
   * *******************************************************
   *    Decision variables for students in terms of days
   * *******************************************************
   */

  // All variables for written exams
  val writtenDaySchedule: Map[String, CPIntVar] = Map()
  for (course <- writtenExams) {
    val decisionVariable = CPIntVar(0 until data.nDays, name = course)
    writtenDaySchedule += course -> decisionVariable
  }

  // All variables for students
  val studentDaySchedule: Map[String, Map[String, CPIntVar]] = Map()
  for (student <- data.coursesFollowedBy.keys) {
    val currentCoursesForStudent: Map[String, CPIntVar] = Map()

    for (course <- data.coursesFollowedBy(student)) {
      var decisionVarForCourse: CPIntVar = null;

      if (writtenExams.contains(course))
        decisionVarForCourse = writtenDaySchedule(course)

      else if (oralExams.contains(course)) {
        decisionVarForCourse = CPIntVar(0 until data.nDays, name = course + " for " + student)
      }
      currentCoursesForStudent += (course -> decisionVarForCourse)
    }
    studentDaySchedule += student -> (currentCoursesForStudent)
  }

  /**
   * *******************************************************
   *    Model variables for students in terms of days
   * *******************************************************
   */

  // Contains space between all written courses (same for all students)
  val daysBetweenWrittenCourses: Map[(String, String), CPIntVar] = Map()

  // Contains space between followed courses of each students
  val daysBetweenCoursesOf: Map[String, Map[(String, String), CPIntVar]] = Map()

  // Contains minimal day gap for each students
  val minimalConsecutiveFreeDaysFor: Map[String, CPIntVar] = Map()

  /**
   * We populate the model variable with days in which student passe exams
   */
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1)) {
    val distancesForStudent = Map[(String, String), CPIntVar]()
    for (c1 <- data.coursesFollowedBy(student); c2 <- data.coursesFollowedBy(student); if (c1 < c2)) {

      // If both exams are written we put a reference of the var from the written table
      if (writtenExams.contains(c1) && writtenExams.contains(c2)) {
        if (!daysBetweenWrittenCourses.contains(c1, c2))
          daysBetweenWrittenCourses += (c1, c2) -> CPIntVar(0 until data.nDays, name = "Days between exams " + c1 + " " + c2)
        distancesForStudent += (c1, c2) -> daysBetweenWrittenCourses(c1, c2)
      } else
        distancesForStudent += (c1, c2) -> CPIntVar(0 until data.nDays, name = "Days between exams " + c1 + " " + c2)

    }
    daysBetweenCoursesOf += student -> distancesForStudent
  }

  /**
   * We populate the var representing the minimal Day gap for each student
   */
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1))
    minimalConsecutiveFreeDaysFor += student -> CPIntVar(0 until data.nDays, name = "Minimal number of free days between 2 exams for " + student)

  /**
   * *******************************************************
   *              Linking constraints
   * *******************************************************
   */

  /**
   * C10 : Linking session scheduling with day scheduling
   */
  for (student <- data.coursesFollowedBy.keys)
    for (course <- studentDaySchedule(student).keys)
      add(or(
        (0 until data.sessionsPerDay).map(session => studentDaySchedule(student)(course) * data.sessionsPerDay + session === studentSchedule(student)(course)(0))))

  /**
   * C11 : Compute the day gap between each pair of exam for each student
   */
  for (student <- data.students)
    for (c1 <- data.coursesFollowedBy(student); c2 <- data.coursesFollowedBy(student); if (c1 < c2))
      add(daysBetweenCoursesOf(student)(c1, c2) == (studentDaySchedule(student)(c1) - studentDaySchedule(student)(c2)).abs)

  /**
   * C12 : Retrieve the minimal day gap between exams for each student
   */
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1))
    add(minimum(daysBetweenCoursesOf(student).values.toArray, minimalConsecutiveFreeDaysFor(student)))

  cp.addDecisionVariables(daysBetweenCoursesOf.values.flatMap(_.values))
  cp.addDecisionVariables(writtenDaySchedule.values)

  /**
   * *******************************************************
   *                 Printing functions
   * *******************************************************
   */
  def printDayStats {
    println("[STATS] ==============================================================")
    println("[STATS]             Number of free days between exams")
    println("[STATS] ==============================================================")
    val range = 0 until data.nDays
    val students = data.students.filter(student => data.coursesFollowedBy(student).size > 1)
    val res = range.map(nMin => (nMin, students.filter(student => minimalConsecutiveFreeDaysFor(student).value == nMin).size))
    res.map(elem => if (elem._2 > 0) println("[STATS] " + elem._1 + " days for " + elem._2 + " different students"))
    println("[STATS] ==============================================================")
  }

  def printOptimizationOnDaysGlobalGap {
    val a = minimalConsecutiveFreeDaysFor.values.map(_.value)
    val b = a.count(_ == a.min)
    println(data.students.size * a.min - b)
  }
}