package solver

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import scala.collection.mutable.Map
import scala.collection.mutable.Set
import akka.event.LoggingAdapter
import akka.event.Logging

class ProblemData {
  var nDays: Int = 24 // default value
  var sessionsPerDay: Int = 2 // default value
  var firstDay = 0 // default value is monday
  /**
   * number of sessions for the exam period (= nDays * sessionsPerDay)
   */
  var nSession = () => { this.nDays * this.sessionsPerDay }

  /**
   * Map representing the courses followed by a student s:
   * <em>coursesFollowedBy(s) === Set of course names followed by s</em>
   */
  val coursesFollowedBy = Map[String, Set[String]]().withDefaultValue(Set())

  /**
   * Map representing the students following an oral course c:
   * <em>studentFollowingCourse(c) === Set of student names following course c</em>
   */
  val studentFollowingCourse = Map[String, Set[String]]().withDefaultValue(Set())

  /**
   * Map representing the set of professors giving a course c:
   * <em>professorsGivingCourse(c) === Set of professor names giving c </em>
   */
  val professorsGivingCourse = Map[String, Set[String]]().withDefaultValue(Set())

  /**
   * Map representing the set of courses given by a professor p:
   * <em>courseGivenByProfessor(p) === Set of courses names given by p </em>
   */
  val courseGivenByProfessor = Map[String, Set[String]]().withDefaultValue(Set())

  /**
   * Map representing the set of orals given by a professor p:
   * <em>oralsGivenByProfessor(p) === Set of oral exams given by p </em>
   */
  val oralsGivenByProfessor = Map[String, Set[String]]().withDefaultValue(Set())

  /**
   * Map representing the set of written exams given by a professor p:
   * <em>writtenGivenByProfessor(p) === Set of written exams names given by p </em>
   */
  val writtenGivenByProfessor = Map[String, Set[String]]().withDefaultValue(Set())

  /**
   * Set of courses composing this instance
   */
  val courses = Set[String]()

  /**
   * Set of students composing this instance
   */
  val students = Set[String]()

  /**
   * Set of professors composing this instance
   */
  val professors = Set[String]()

  /**
   *  Map representing the restriction for course c:
   *  sessionUnavailableFor(c) === Array of sessionId that cannot be assigned to course c
   */
  val sessionUnavailableFor = Map[String, Array[Int]]()

  /**
   *   Map representing the fixation of session s for course c:
   *  sessionFixedFor(c) === s
   */
  val sessionFixedFor = Map[String, Array[Int]]()

  /**
   *  Map representing the number of contiguous slots taken by course c:
   *  numberOfSlotsFor(c) === Number of Contiguous Slots
   */
  val numberOfSlotsFor = Map[String, Int]()

  /**
   *  Map representing the number of non contiguous slots taken by oral exam c
   *  numberOfSlotsForOral(c) === number of possible sessions for oral exam of course c
   */
  val numberOfSlotsForOral = Map[String, Int]()

  /**
   *  Map representing the maximum number of students for an oral session
   *  nStudentPerOralSession(s) === maximum number of students for an oral session
   */
  val nStudentPerOralSession = Map[String, Int]()

  /**
   * **************************************************************************
   *                           UTILITY METHODS
   * **************************************************************************
   */

  /**
   * clear all the model data parameters.
   */
  def resetInstanceVariables {
    coursesFollowedBy.clear()
    studentFollowingCourse.clear()
    professorsGivingCourse.clear()
    courseGivenByProfessor.clear()
    oralsGivenByProfessor.clear()
    writtenGivenByProfessor.clear()
    courses.clear()
    students.clear()
    professors.clear()
    sessionUnavailableFor.clear()
    sessionFixedFor.clear()
    numberOfSlotsFor.clear()
    numberOfSlotsForOral.clear()
    nStudentPerOralSession.clear()
  }

  /**
   * Uses println to log the metrics
   */
  def printMetrics { printMetrics(Right(println)) }

  /**
   * uses the provided logger to print the metrics (concurent enabled)
   * @param log (LoggingAdapter) the logger to use (must define a .info(string) method) !!
   */
  def logMetrics(log: LoggingAdapter) { printMetrics(Left(log)) }
  /**
   * Print instance metrics
   */
  def printMetrics(log: Either[LoggingAdapter, (String) => Unit]) {

    var sum = 0
    for (student <- students)
      sum += coursesFollowedBy(student).size

    val multislotsExams = numberOfSlotsFor.keySet
    val oralExams = numberOfSlotsForOral.keySet
    val writtenExams = writtenGivenByProfessor.values.toSet
    val unislotExams = courses.diff(multislotsExams).diff(oralExams)

    //val highNofCourse = coursesFollowedBy.keys.map(item => if (coursesFollowedBy(item).size > 7) {println(item + " with " +  coursesFollowedBy(item).size + "courses"); item})
    //println("Précédemment : " + highNofCourse.size + " étudiants")

    val maxEnrollement = coursesFollowedBy.values.map(_.size).max

    val msg = "\n[INFO] ====================================================================================================" +
      "\n[INFO] \t\t\t\t\t  Problem Metrics" +
      "\n[INFO] ====================================================================================================" +
      "\n[INFO] #students:  \t\t\t\t" + students.size +
      "\n[INFO] #courses:  \t\t\t\t" + courses.size +
      "\n[INFO] #profs:  \t\t\t\t" + courseGivenByProfessor.keys.size +
      "\n[INFO] #normal courses:  \t\t\t" + unislotExams.size +
      "\n[INFO] #multislots courses:  \t\t\t" + multislotsExams.size + " (" + multislotsExams.mkString(", ") + ")" +
      "\n[INFO] #oral courses:  \t\t\t\t" + oralExams.size + " (" + oralExams.mkString(", ") + ")" +
      "\n[INFO] Average number of exam per student: \t" + (sum.toDouble / students.size.toDouble).round +
      "\n[INFO] Average number of exam per professor: \t" + courseGivenByProfessor.values.map(_.size).sum / courseGivenByProfessor.keys.size.toDouble +
      "\n[INFO] Maximum course enrollement: " + maxEnrollement +
      "\n[INFO] ===================================================================================================="
    log match {
      case Left(logger) => logger.info(msg)
      case Right(println) => println(msg)
    }
  }

  /**
   * Export data into an easier to read format
   * studentID\tcourse1 course2 course3 etc...
   */
  def printParsedData {

    val file = new File("data/data.csv")
    val bw = new BufferedWriter(new FileWriter(file))

    val mapCourseInt: Map[String, Int] = Map()
    var counter = 0
    for (course <- courses) {
      mapCourseInt += course -> counter
      counter += 1
    }
    var studId = 0
    for (student <- students) {
      val line = studId + "\t" + coursesFollowedBy(student).map(course => mapCourseInt(course)).mkString(" ")
      bw.write(line + "\n")
      studId += 1
    }

    bw.close()

  }
}