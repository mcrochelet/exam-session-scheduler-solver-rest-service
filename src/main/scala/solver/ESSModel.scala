package solver

import oscar.cp._
import oscar.algo.search.SearchStatistics
import scala.collection.mutable._
import oscar.util._
import oscar.cp.constraints.CountCst

class ESSModel(val data: ProblemData) extends CPModel {

  val cp = solver
  cp.silent = true

  var lastSolutionMetrics: SearchStatistics = null
  var onSolutionCallback: (Map[String, Array[Int]], scala.collection.Seq[(Int, Int)]) => Unit = null

  def isSunday(session: Int) = (session + data.firstDay * data.sessionsPerDay) % (data.sessionsPerDay * 7) >= 6 * data.sessionsPerDay

  val nSession = data.nSession()

  /**
   *  All possible sessions
   */
  val rSession = (0 until nSession).filter(!isSunday(_))

  /**
   * Array of all courses
   */
  val courseArray = data.courses.toArray

  val multislotsExams = data.numberOfSlotsFor.keys.toSet
  val oralExams = data.numberOfSlotsForOral.keys.toSet
  val unislotExams = data.courses.diff(multislotsExams).diff(oralExams)
  val writtenExams = multislotsExams ++ unislotExams

  def isPossibleSlotForBeginningOfMultislotExamOf(numberOfSlots: Int, slot: Int) =
    slot % data.sessionsPerDay <= data.sessionsPerDay - numberOfSlots

  /**
   * **************************************************************************
   *                          Decision variables
   * **************************************************************************
   */

  /**
   * All decision variables gathered when created in this buffer in order to launch the search
   */
  val decisionVariables: Buffer[CPIntVar] = Buffer()
  val decisionVariablesNames: Buffer[String] = Buffer()

  /**
   * Decision variables for courses uni- and multi- slots exams (no orals !!!)
   */
  val unislotsSchedule: Map[String, CPIntVar] = Map()
  for (course <- unislotExams) {
    val decisionVariable = CPIntVar(rSession, name = course)
    unislotsSchedule += course -> decisionVariable
    decisionVariables += decisionVariable
    decisionVariablesNames += course
  }

  val multislotsSchedule: Map[String, Array[CPIntVar]] = Map()
  for (course <- multislotsExams) {
    val slotsForFirstPeriod = rSession.filter(slot => isPossibleSlotForBeginningOfMultislotExamOf(data.numberOfSlotsFor(course), slot))
    if (slotsForFirstPeriod.isEmpty) {
      println("Too many sessions (" + data.numberOfSlotsFor(course) + ") are asked for course " + course + ". " + data.sessionsPerDay + " is the maximum allowed !");
      System.exit(-1)
    }
    val firstSlot = CPIntVar(slotsForFirstPeriod, name = course)
    val decisionVariable = Array(firstSlot) ++: Array.tabulate(data.numberOfSlotsFor(course) - 1) { index => firstSlot + (index + 1) }
    multislotsSchedule += course -> decisionVariable
    decisionVariables ++= decisionVariable.toBuffer
    decisionVariablesNames ++= Array.fill(data.numberOfSlotsFor(course))(course)
  }

  val oralsSchedule: Map[String, Array[CPIntVar]] = Map()
  for (course <- oralExams) {
    val decisionVariable = Array.tabulate(data.numberOfSlotsForOral(course)) { index => CPIntVar(rSession, name = course + " session " + index) }
    // Two follwoing lines allows to group slots of oral examinations
    //val firstSlot = CPIntVar(rSession, name = course)
    //val decisionVariable = Array(firstSlot) ++: Array.tabulate(data.numberOfSlotsForOral(course) - 1) { index => firstSlot + (index + 1) }
    oralsSchedule += course -> decisionVariable
    decisionVariables ++= decisionVariable.toBuffer
    decisionVariablesNames ++= Array.fill(data.numberOfSlotsForOral(course))(course)
  }

  /**
   * Decision variables for students in terms of sessions
   */
  val studentSchedule: Map[String, Map[String, Array[CPIntVar]]] = Map()
  for (student <- data.coursesFollowedBy.keys) {
    val currentCoursesForStudent: Map[String, Array[CPIntVar]] = Map()

    for (course <- data.coursesFollowedBy(student)) {
      var decisionVarForCourse = Array[CPIntVar]()

      if (unislotExams.contains(course))
        decisionVarForCourse = Array(unislotsSchedule(course))

      else if (multislotsExams.contains(course))
        decisionVarForCourse = multislotsSchedule(course)

      else if (oralExams.contains(course)) {
        decisionVarForCourse = Array(CPIntVar(rSession, name = course + " for " + student))
        decisionVariables ++= decisionVarForCourse.toBuffer
        decisionVariablesNames += course
      }
      currentCoursesForStudent += (course -> decisionVarForCourse)
    }
    studentSchedule += student -> (currentCoursesForStudent)
  }

  /**
   * Map from an oral course to CPIntVar of all students following this course
   *
   */
  val oralForStudentsSchedule: Map[String, Array[CPIntVar]] = Map()
  for (course <- oralExams) {
    val studentThatFollowCourse: Buffer[CPIntVar] = Buffer()

    for (student <- data.students; if data.coursesFollowedBy(student).contains(course))
      studentThatFollowCourse += studentSchedule(student)(course)(0)

    oralForStudentsSchedule += course -> studentThatFollowCourse.toArray
  }

  /**
   * Return an array with the CPIntVar of courseId's sessions
   *
   */
  def getCPIntOfCourse(courseId: String): Array[CPIntVar] = {
    if (oralExams.contains(courseId))
      return oralsSchedule(courseId)
    else if (unislotExams.contains(courseId))
      return Array(unislotsSchedule(courseId))
    else
      return multislotsSchedule(courseId)
  }

  /**
   * **************************************************************************
   *                          Model variables
   * **************************************************************************
   */

  val studentViolation: Map[String, CPIntVar] = Map()
  for (student <- data.students)
    studentViolation += student -> CPIntVar(0 until data.coursesFollowedBy(student).size, name = "Violation for " + student)

  /**
   * Number of sessions between pairs of courses followed by each student
   */
  // Contains space between all written courses (same for all students)
  val spaceBetweenWrittenCourses: Map[(String, String), CPIntVar] = Map()

  // Contains space between followed courses of each students
  val spaceBetweenCoursesOf: Map[String, Map[(String, String), CPIntVar]] = Map()

  // We iter on all students having more than 1 exam
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1)) {
    val distancesForStudent = Map[(String, String), CPIntVar]()
    for (c1 <- data.coursesFollowedBy(student); c2 <- data.coursesFollowedBy(student); if (c1 < c2)) {

      // If both exams are written we put a reference of the var from the written table
      if (writtenExams.contains(c1) && writtenExams.contains(c2)) {
        if (!spaceBetweenWrittenCourses.contains(c1, c2))
          spaceBetweenWrittenCourses += (c1, c2) -> CPIntVar(0 until nSession, name = "Days between exams " + c1 + " " + c2)
        distancesForStudent += (c1, c2) -> spaceBetweenWrittenCourses(c1, c2)
      } else
        distancesForStudent += (c1, c2) -> CPIntVar(0 until nSession, name = "Days between exams " + c1 + " " + c2)

    }
    spaceBetweenCoursesOf += student -> distancesForStudent
  }

  val minimalConsecutiveFreeSessionFor: Map[String, CPIntVar] = Map()
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1))
    minimalConsecutiveFreeSessionFor += student -> CPIntVar(0 until nSession, name = "Minimal number of free session between 2 exams for " + student)

  /**
   * **************************************************************************
   *                              Constraints
   * **************************************************************************
   */

  /**
   * C1 : Each and every student has all his exams in different sessions
   */
  for (student <- data.students)
    add(softAllDifferent(studentSchedule(student).values.flatten.toArray, studentViolation(student)))

  /**
   * C2 : Slots in which a student present an oral exam must be one of the slot in which the oral exam is planned
   *      Linking decision variable of students with decision variables for oral exams
   */
  for (student <- data.students)
    for (course <- data.coursesFollowedBy(student); if (oralExams.contains(course)))
      add(or(oralsSchedule(course).map {
        potentialSession => potentialSession === studentSchedule(student)(course)(0)
      }))

  /**
   * C3 : All oral session slots must be different for a specific course.
   */
  for (course <- oralExams)
    add(allDifferent(oralsSchedule(course)))

  /**
   * C3bis : All non assigned slot must be in a strictly ascending order.
   * This constraint breaks the symmetry between slots for oral examination
   * Swapping the allocated slots for different oral examination (of a same course)
   *
   */
  for ((exam, sessions) <- data.sessionFixedFor)
    for (i <- sessions.size + 1 until getCPIntOfCourse(exam).size)
      add(getCPIntOfCourse(exam)(i) < sessions(i))

  /**
   * C4 : Sessions assignation
   *      Some exam are assignated to specific sessions
   *
   */
  for ((exam, sessions) <- data.sessionFixedFor)
    if ((oralExams.contains(exam) || sessions.size == 1) && data.numberOfSlotsForOral(exam) >= sessions.size)
      for (i <- 0 until sessions.size)
        add(getCPIntOfCourse(exam)(i) == sessions(i))
    else
      println(exam + "tries to block " + sessions.size + " slots, but only 1 can be blocked")

  /**
   * C5 : Session restriction
   *      Some exam are restricted to specific sessions
   */
  for ((exam, sessions) <- data.sessionUnavailableFor)
    for (restrictedSession <- sessions; examSession <- getCPIntOfCourse(exam))
      add(examSession != restrictedSession)

  /**
   * C6 : A written exam cannot be in same session than an oral given by the same prof
   *      (full availability of the prof for the oral exam)
   */
  for (profId <- data.writtenGivenByProfessor.keys) {
    val oralVars = data.oralsGivenByProfessor.getOrElse(profId, Set())
      .flatMap(oralsSchedule(_))

    for (written <- data.writtenGivenByProfessor(profId)) {
      val writtenVar = getCPIntOfCourse(written)
      add(allDifferent(oralVars ++ writtenVar))
    }
  }

  /**
   * C7 : Ensure that number of student presenting oral in session i [0..nSession[ is belongs to range [0, nStudentPerOralSession[
   */
  for (course <- oralExams)
    add(gcc(oralForStudentsSchedule(course), 0 until nSession, Array.fill(nSession)(0), Array.fill(nSession)(data.nStudentPerOralSession(course))))

  /**
   * C8 :Compute the number of sessions between each pair of exam of each student
   */
  for (student <- data.students)
    for (c1 <- data.coursesFollowedBy(student); c2 <- data.coursesFollowedBy(student); if (c1 < c2)) {
      val distance = Array(studentSchedule(student)(c1)(0) - studentSchedule(student)(c2).last,
        studentSchedule(student)(c2)(0) - studentSchedule(student)(c1).last,
        CPIntVar(0))
      add(maximum(distance, spaceBetweenCoursesOf(student)(c1, c2)))
    }

  /**
   * C9 : Retrieve the minimal session gap between exams for each student
   */
  for (student <- data.students; if (data.coursesFollowedBy(student).size > 1))
    add(minimum(spaceBetweenCoursesOf(student).values.toArray, minimalConsecutiveFreeSessionFor(student)))

  /**
   * **************************************************************************
   *                     Print functions
   * **************************************************************************
   */

  /**
   * Print solution for all the students
   */
  def printResultForStudents {
    for (student <- data.students)
      printResultForStudent(student)
  }

  /**
   * Format output for the exams of one student
   */
  def printResultForStudent(student: String = data.students.head) {
    println("Student `" + student + "`")
    println(studentSchedule(student).map {
      case (course, session) => "\t -> " + course + " on session " + session.map(s => s.value).toSeq.mkString(" ")
    }.mkString("\n"))
  }

  /**
   * Prints some statistics
   */
  def printSessionStats {
    println("[STATS] ==============================================================")
    println("[STATS]             Number of free sessions between exams")
    println("[STATS] ==============================================================")
    val range = 0 until nSession
    val students = data.students.filter(student => data.coursesFollowedBy(student).size > 1)
    val res = range.map(nMin => (nMin, students.filter(student => minimalConsecutiveFreeSessionFor(student).value == nMin).size))
    res.map(elem => if (elem._2 > 0) println("[STATS] " + elem._1 + " sessions for " + elem._2 + " different students"))
    println("[STATS] ==============================================================")
  }

  def printOptimizationOnSessionGlobalGap {
    val a = minimalConsecutiveFreeSessionFor.values.map(_.value)
    val b = a.count(_ == a.min)
    println(data.students.size * a.min - b)
  }

}