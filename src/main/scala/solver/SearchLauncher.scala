package solver
import oscar.cp._
import scala.util._
import scala.collection.mutable.Map
/**
 * List of Searches :
 *        - RandomRelaxation
 *        - RandomAdaptativeRelaxation
 *        - WorstCourseAndRandomRelaxation
 *
 * List of Optimization functions :
 *        - OptimizationFunctionWS      (Basic Weighed Sum)
 *        - OptimizationFunctionVOLNS   (A basic VO LNS)
 *        - OptimizationFunction        (A more complex VO LNS)
 *
 */

trait SearchLauncher extends WorstCourseAndRandomRelaxation {
  println("search launched")

  search {
    val variables = decisionVariables.toArray
    val rand = Random

    //binaryFirstFail(decisionVariables)

    /**
     * Randomized variable heuristic
     */
    binaryLastConflict(variables, i => variables(i).size, variables(_).randomValue(rand))

    /**
     * variable heuristic on course size
     */
    //binaryLastConflict(variables, i => data.studentFollowingCourse(decisionVariablesNames(i)).size, variables(_).randomValue)

    /**
     * variable heuristic on orals/written
     */
    //binaryLastConflict(variables, i => if (oralExams.contains(decisionVariablesNames(i))) 0 else 1, variables(_).randomValue)

  }

  /**
   * Ad-hoc comportment when a solution is found
   */
  onSolution {
    printOptimizationOnDaysGlobalGap
    //printResultForStudents
    val solution = Map[String, Array[Int]]()
    unislotsSchedule.map { case (k, v) => { solution += k -> Array(v.value) } }
    multislotsSchedule.map { case (k, v) => { solution += k -> v.map(_.value) } }
    oralsSchedule.map { case (k, v) => { solution += k -> v.map(_.value) } }
    val range = 0 until data.nDays
    val students = data.students.filter(student => data.coursesFollowedBy(student).size > 1)
    val res = range.map(nMin => (nMin, students.count(student => minimalConsecutiveFreeDaysFor(student).value == nMin)))

    onSolutionCallback(solution, res)
  }
}