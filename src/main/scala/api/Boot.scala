package api

import akka.actor._
import akka.io.IO
import akka.pattern.ask
import api.routing._
import spray.can._

import scala.collection.mutable

/**
 * Main launcher of the REST server. Reference in the build.sbt
 * Its task is to simply create the router, the map of instances and bind
 * the router to the interface.
 * It uses (implicit) variables defined globally for the package in api/package.scala
 */
object Boot extends App {

  /**
   * Map of the actors instances referenced by their names.
   */
  val instances = mutable.Map[instanceName, ActorRef]()

  // instantiate router and bind it to local interface
  val master = system.actorOf(Props(Router(instances)), "Router")
  IO(Http) ? Http.Bind(master, interface = "localhost", port = 8080)
}