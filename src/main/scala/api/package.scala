import akka.actor.{Props, DeadLetter, Actor, ActorSystem}
import akka.event.Logging
import akka.util.Timeout

import scala.concurrent.duration.DurationInt

/**
 * Created by mcrochelet on 12/05/15.
 * Defines the global variables that are needed in the whole package
 */
package object api {
  // simple type definition for readability
  type instanceName = String

  /**
   * Global system of the REST api
   */
  implicit val system = ActorSystem("ess-RESTServer")
  /**
   * <emph>Global</emph> timeout to be used in the whole server
   */
  implicit val timeout = Timeout(5.seconds)

  /**
   * Global execution context used for ask methods (actor.?(method))
   */
  implicit val ec = system.dispatcher

  /**
   * Log level for the entire application, can be either Debug, Info, Error or Warning Level
   */
  private val logLevel = Logging.InfoLevel

  /**
   * Bacis implementation of a listener actor whose role is to log any
   * deadletter (see <a href="http://doc.akka.io/docs/akka/snapshot/general/message-delivery-reliability.html)"></a>
   */
  private class Listener extends Actor {
    val log = Logging(context.system, this)

    def receive = {
      case d: DeadLetter => log.debug(d.message.toString)
    }
  }

  // only instantiate actor if it will log something
  if (logLevel == Logging.DebugLevel) {
    val listener = system.actorOf(Props[Listener], "deadletterListener")
    system.eventStream.subscribe(listener, classOf[DeadLetter])
  }

  // set the global log level.
  system.eventStream.setLogLevel(logLevel)
}
