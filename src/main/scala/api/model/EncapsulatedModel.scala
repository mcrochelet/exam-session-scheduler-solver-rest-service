package api.model

import akka.actor.{Actor, actorRef2Scala}
import akka.event.Logging
import solver.{ESSModel, ProblemData, SearchLauncher}
import util._

import scala.collection.mutable.Map

/**
 * @author mcrochelet
 * Encapsulate the cp model with a concurrent safe interface and adds data required by the REST interface such as
 *  - ids for resources (students, courses, etc.)
 *  - callback
 *  the protocol is defined in object EncapsulatedModelProtocol
 */
class EncapsulatedModel extends Actor {
  val log = Logging(context.system, this)
  private val model = new ProblemData()
  /*                              constraint type, course name*/
  val constraints      = Map[Int, (String, String)]()
  var constraintId     = 0

  val students         = Map[Int, String]()
  var studentId        = 0

  val courses          = Map[Int, String]()
  var courseId         = 0

  val professors       = Map[Int, String]()
  var professorId      = 0

  var callbackAddress  = ("notset", 80, "")

  import EncapsulatedModelProtocol._

  private var searchLaunched = false

  def receive = {
    case setNumberOfDays(n)          => model.nDays = n
    case getNumberOfDays()           => sender ! model.nDays

    case setFirstDay(n)          => model.firstDay = n
    case getFirstDay()           => sender ! model.firstDay

    case setNumberOfSessionPerDay(n) => model.sessionsPerDay = n
    case getNumberOfSessionPerDay()  => sender ! model.sessionsPerDay

    case setCallbackAddress(cb, port, r) => callbackAddress = (cb, port, r)
    case getCallbackAddress() => sender ! callbackAddress

    case isSearchLaunched() => sender ! searchLaunched

    case addStudent(s)               => {
      val s0 = model.students.size // trick to avoid parsing the whole array freely (model.students is a set)
      model.students += s
      var sId = studentId
      // if set size has moved, the students did not already exist in the model -> needs an id
      if (s0 < model.students.size) {
        log.debug(s"creating student $s")
        students += studentId -> s
        studentId += 1
      } else sId = students.keysOf(s)(0)
      // send the iD of newly created student back
      sender ! sId
    }
    case removeStudent(sId)          => if (students.contains(sId)) {
      log.debug(s"removing student ${students(sId)}")
      model.students -= students(sId)
      for (c <- model.coursesFollowedBy(students(sId))) model.studentFollowingCourse -= c -> students(sId)
      model.coursesFollowedBy -= students(sId)
      students -= sId
    }
    case updateStudent(sId, s)       => if (students.contains(sId)) {
      log.debug(s"updating student ${students(sId)} to $s")
      model.students -= students(sId)
      model.students += s
      for (c <- model.coursesFollowedBy(students(sId))) {
        model.studentFollowingCourse -= c -> students(sId)
        model.studentFollowingCourse += c -> s
      }
      model.coursesFollowedBy += s -> model.coursesFollowedBy(students(sId))
      model.coursesFollowedBy -= students(sId)
      students(sId) = s
      sender ! true
    } else sender ! false
    case getStudents()               => sender ! model.students
    case getStudentOfId(sId)         => sender ! students.getOrElse(sId,"")

    case addProfessor(p)             => {
      val p0 = model.professors.size
      model.professors += p
      var pID = professorId
      if (p0 < model.professors.size) {
        log.debug(s"creating professor $p")
        professors += professorId -> p
        professorId += 1
      } else pID = professors.keysOf(p)(0)
      sender ! pID
    }
    case removeProfessor(pId)        => if (professors.contains(pId)){
      log.debug(s"removing prof ${professors(pId)}")
      model.professors -= professors(pId)

      for (c <- model.courseGivenByProfessor(professors(pId)))
        model.professorsGivingCourse -= c -> professors(pId)
      model.courseGivenByProfessor -= professors(pId)

      model.oralsGivenByProfessor -= professors(pId)
      model.writtenGivenByProfessor -= professors(pId)

      professors -= pId
    }
    case updateProfessor(pId, p)     => if (professors.contains(pId)) {
      log.debug(s"updating prof ${professors(pId)} to $p")
      model.professors -= professors(pId)
      model.professors += p

      for (c <- model.courseGivenByProfessor(professors(pId))) {
        model.professorsGivingCourse += c -> p
        model.professorsGivingCourse -= c -> professors(pId)
      }
      model.courseGivenByProfessor += p -> model.courseGivenByProfessor(professors(pId))
      model.courseGivenByProfessor -= professors(pId)

      model.oralsGivenByProfessor += p -> model.oralsGivenByProfessor(professors(pId))
      model.oralsGivenByProfessor -= professors(pId)
      model.writtenGivenByProfessor += p -> model.writtenGivenByProfessor(professors(pId))
      model.writtenGivenByProfessor -= professors(pId)

      professors(pId) = p
      sender ! true
    } else sender ! false
    case getProfessors()             => sender ! model.professors
    case getProfessorOfId(pId)       => sender ! professors.getOrElse(pId, "")

    case addCourse(c)                => {
      val c0 = model.courses.size
      model.courses += c
      var cId = courseId
      if (c0 < model.courses.size) {
        log.debug(s"creating course $c")
        courses += courseId -> c
        courseId += 1
      } else cId = courses.keysOf(c)(0)
      sender ! cId
    }
    case removeCourse(c)             => if (courses.contains(c)) {
      log.debug(s"removing course ${courses(c)}")
      model.courses -= courses(c)

      // lien prof <-> course
      for (p <- model.professorsGivingCourse(courses(c))) {
        model.courseGivenByProfessor -= p -> courses(c)
        model.oralsGivenByProfessor -= p -> courses(c)
        model.writtenGivenByProfessor -= p -> courses(c)
      }
      model.professorsGivingCourse -= courses(c)

      // lien student <-> course
      for (s <- model.studentFollowingCourse(courses(c)))
        model.coursesFollowedBy -= s -> courses(c)

      model.studentFollowingCourse -= courses(c)

      // lien constraint <-> course
      model.sessionUnavailableFor -= courses(c)
      model.sessionFixedFor -= courses(c)
      model.numberOfSlotsFor -= courses(c)
      model.numberOfSlotsForOral -= courses(c)
      model.nStudentPerOralSession -= courses(c)

      courses -= c
    }
    case updateCourse(cId, c)        => if (courses.contains(cId)) {
      log.debug(s"updating course ${courses(cId)} to $c")
      model.courses -= courses(cId)
      model.courses += c

      // lien prof <-> cours
      for (p <- model.professorsGivingCourse(courses(cId))) {
        model.courseGivenByProfessor -= p -> courses(cId)
        model.courseGivenByProfessor += p -> c
        model.oralsGivenByProfessor -= p -> courses(cId)
        model.oralsGivenByProfessor += p -> c
        model.writtenGivenByProfessor -= p -> courses(cId)
        model.writtenGivenByProfessor += p -> c
      }
      model.professorsGivingCourse += c -> model.professorsGivingCourse(courses(cId))
      model.professorsGivingCourse -= courses(cId)

      // lien student <-> course
      for (s <- model.studentFollowingCourse(courses(cId))) {
        model.coursesFollowedBy -= s -> courses(cId)
        model.coursesFollowedBy -= s -> c
      }
      model.studentFollowingCourse += c -> model.studentFollowingCourse(courses(cId))
      model.studentFollowingCourse -= courses(cId)

      // lien constraint <-> course
      model.sessionUnavailableFor += c -> model.sessionUnavailableFor(courses(cId))
      model.sessionUnavailableFor -= courses(cId)
      model.sessionFixedFor += c -> model.sessionFixedFor(courses(cId))
      model.sessionFixedFor -= courses(cId)
      model.numberOfSlotsFor += c -> model.numberOfSlotsFor(courses(cId))
      model.numberOfSlotsFor -= courses(cId)
      model.numberOfSlotsForOral += c -> model.numberOfSlotsForOral(courses(cId))
      model.numberOfSlotsForOral -= courses(cId)
      model.nStudentPerOralSession += c -> model.nStudentPerOralSession(courses(cId))
      model.nStudentPerOralSession -= courses(cId)

      courses(cId) = c
      sender ! true
    } else sender ! false
    case getCourses()                => sender ! model.courses
    case getCourseOfId(cId)          => sender ! courses.getOrElse(cId, "")

    case addStudentFollowingCourse(sId, cId) => {
      if (students.contains(sId) && courses.contains(cId)) {
        log.debug(s"adding $sId following $cId")
        model.studentFollowingCourse += courses(cId) -> students(sId)
        model.coursesFollowedBy += students(sId) -> courses(cId)
        sender ! true
      } else sender ! false
    }
    case removeStudentFollowingCourse(sId, cId) => {
      if (students.contains(sId) && courses.contains(cId)) {
        log.debug(s"removing $sId following $cId")
        model.studentFollowingCourse -= courses(cId) -> students(sId)
        model.coursesFollowedBy -= students(sId) -> courses(cId)
        sender ! true
      } else sender ! false
    }
    case getStudentsFollowingCourse(cId) => if (courses.contains(cId)) sender ! model.studentFollowingCourse(courses(cId))
    case getCoursesFollowedByStudent(sId) => if (students.contains(sId)) sender ! model.coursesFollowedBy(students(sId))

    case addProfessorGivingCourse(p, c) => {
      if (professors.contains(p) && courses.contains(c)) {
        log.debug(s"adding $p giving $c")
        model.professorsGivingCourse += courses(c) -> professors(p)
        model.courseGivenByProfessor += professors(p) -> courses(c)
        sender ! true
      } else sender ! false
    }
    case removeProfessorGivingCourse(p, c) => {
      if (professors.contains(p) && courses.contains(c)) {
        log.debug(s"removing $p giving $c")
        model.professorsGivingCourse -= courses(c) -> professors(p)
        model.courseGivenByProfessor -= professors(p) -> courses(c)
        sender ! true
      } else sender ! false
    }
    case getProfessorsTeachingCourse(cId) => if (courses.contains(cId)) sender ! model.professorsGivingCourse(courses(cId))
    case getCoursesTeachedByProfessor(pId) => if (professors.contains(pId)) sender ! model.courseGivenByProfessor(students(pId))

    case addESSConstraint(constraintType, args) => {
      var correctType = true
      constraintType match {
        case "fixed" => args match {
          case examId :: sessions => {
            log.info(s"Fixing session ${sessions.mkString(", ")} for exam $examId")
            model.sessionFixedFor += examId -> sessions.toArray.map(_.toInt)
          }
          case _ => log.error("addESSConstraint :: not enough arguments for fixed constraint")
        }
        case "unavailable" => args match {
          case examId :: sessions => {
            log.info(s"Preventing exam $examId to take sessions: (${sessions.mkString(", ")})")
            model.sessionUnavailableFor += examId -> sessions.toArray.map(_.toInt)
          }
          case _ => log.error("addESSConstraint :: not enough arguments for restricted constraint")
        }
        case "prof-unavailable" => args match {
          case profId :: sessions => {
            if (model.courseGivenByProfessor.contains(profId)) {
              log.info(s"Professor $profId unavailable for sessions: (${sessions.mkString(", ")})")
              for (c <- model.courseGivenByProfessor(profId)) model.sessionUnavailableFor += c -> sessions.toArray.map(_.toInt)
            }
          }
          case _ => log.error("addESSConstraint :: not enough arguments for restricted constraint")
        }
        case "oral" => args match {
          case examId :: nSlots :: nStudentPerSession :: Nil => {
            log.info(s"Modifying exam for course $examId to be an oral of $nSlots sessions with $nStudentPerSession students per session")
            for (prof <- model.professorsGivingCourse(examId)) model.oralsGivenByProfessor += prof -> examId
            model.numberOfSlotsForOral += examId -> nSlots.toInt
            model.nStudentPerOralSession += examId -> nStudentPerSession.toInt
          }
          case _ => log.error("addESSConstraint :: not enough arguments for oral constraint")
        }
        case "multislot" => args match {
          case examId :: nSlots :: Nil => {
            log.info(s"Modifying exam for course $examId to take $nSlots continuous slots")
            model.numberOfSlotsFor += examId -> nSlots.toInt
          }
          case _ => log.error("addESSConstraint :: not enough arguments for multislot constraint")
        }
        case _ => {
          correctType = false
          log.error("addESSConstraint :: constraint not recognized")
        }
      }
      if (correctType) {
        var cId = constraintId
        val constraintValue = (constraintType, args(0))
        if (!constraints.containsValue(constraintValue)){
          constraints += constraintId -> constraintValue
          constraintId += 1
        } else cId = constraints.keysOf(constraintValue)(0)
        sender ! cId
      } else sender ! -1
    }
    case removeESSConstraint(cId) => {
      if (constraints.contains(cId)) {
        val (constraintType, arg) = constraints(cId)
        constraintType match {
          case "fixed" => {
            log.info(s"Removing Constraint: Fixing session for course $arg")
            var profunavailable = false
            for (p<- model.professorsGivingCourse(arg) if constraints.containsValue(("prof-unavailable", arg))) profunavailable = true
            if (!profunavailable) model.sessionFixedFor -= arg
          }
          case "unavailable" => {
            log.info(s"Removing Constraint: Preventing exam $arg to take some sessions")
            model.sessionUnavailableFor -= arg
          }
          case "oral" => {
            log.info(s"Removing Constraint: Setting $arg to oral exam")
            for (prof <- model.professorsGivingCourse(arg)) model.oralsGivenByProfessor -= prof -> arg
            model.numberOfSlotsForOral -= arg
            model.nStudentPerOralSession -= arg
          }
          case "prof-unavailable" => {
            log.info(s"Removing Constraint: prof unavailable")
            if (model.courseGivenByProfessor.contains(arg)) {
              for (c <- model.courseGivenByProfessor(arg) if !constraints.containsValue(("unavailable", c)))
                model.sessionUnavailableFor -= c
            }
          }
          case "multislot" => {
            log.info(s"Removing Constraint: Setting $arg to multislot")
            model.numberOfSlotsFor -= arg
          }
          case _ => log.error("removeESSConstraint :: constraint not recognized")
        }
        constraints -= cId
      }
    }

    case PrintInstanceMetrics => model.logMetrics(log)

    case ClearData => model.resetInstanceVariables

    case search(onSolution, maxTime) => {
      searchLaunched = true
      for ((profId, exams) <- model.courseGivenByProfessor)
        model.writtenGivenByProfessor += profId -> exams.diff(model.oralsGivenByProfessor(profId))

      val solver = new ESSModel(model) with SearchLauncher //with BarplotDays

      val s = solver.startSearch(maxTime, (s: Map[String, Array[Int]], stat: scala.collection.Seq[(Int, Int)]) => {
        log.info("Propagating solution to frontend")
        onSolution(s, stat)
      }, () => {searchLaunched = false})
    }

    case _ => log.error("message not recognized")
  }
}
