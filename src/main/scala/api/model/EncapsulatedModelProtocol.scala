package api.model
import scala.collection.mutable._

object EncapsulatedModelProtocol {

  /**
   * sets the number of days of the session to n
   * @param n (Integer) the number of days of the session
   */
  case class setNumberOfDays(n: Int)

  /**
   * returns the current number of days of the session
   */
  case class getNumberOfDays()

  /**
   * sets the number of sessions per day of the session to n
   * @param n (Integer) the number of sessions per day
   */
  case class setNumberOfSessionPerDay(n: Int)

  /**
   * returns the current number of sessions per day
   */
  case class getNumberOfSessionPerDay()

  /**
   * sets the first day of the session to n
   * @param n (Integer) the first day of the session. 0 <= n <= 6 with 0 defined as Monday and 6 as Sunday
   */
  case class setFirstDay(n: Int)

  /**
   * returns the current first day of the session
   */
  case class getFirstDay()

  /**
   * Sets the webhook address to be used in the onSolution callback
   * The (complete) address will be: "http://"+cb+":"+port+""+r
   * @param cb (String) the base URL of the webhook
   * @param port (Integer) the port of the webhook
   * @param r (String) the relative path of the webhook (instance specific for example)
   */
  case class setCallbackAddress(cb: String, port: Int, r: String)

  /**
   * returns a tuple3 such that: (cb, port, r) where:
   * <ul> <li> cb : (String) the base URL of the webhook </li>
   * <li>port : (Integer) the port of the webhook</li>
   * <li>r : (String) the relative path of the webhook (instance specific for example)</li>
   */
  case class getCallbackAddress()

  /**
   * returns a boolean: true is the search is launched, false otherwise
   */
  case class isSearchLaunched()

  /**
   * adds the student for the session. If the student is already participating in the session,
   * the student is not added.
   * In either case, the ID of the student is returned
   * @param student (String) represents the student name
   */
  case class addStudent(student: String)

  /**
   * removes the student of ID studentID from the session.
   * This method keeps the consistency of the instance.
   * @param studentId (Integer) the identifier of the student to remove
   */
  case class removeStudent(studentId: Int)

  /**
   * updates the student of identifier studentId (Integer) to the new name `student` (String)
   * This method keeps the consistency of the instance.
   * @param studentId (Integer) the identifier of the student to update
   * @param student (String) represents the student name
   */
  case class updateStudent(studentId: Int, student: String)

  /**
   * returns the name (String) of the student of identifier studentId
   * @param studentId the identifier of the student
   */
  case class getStudentOfId(studentId: Int)

  /**
   * returns a set (strings) of all the student names currently participating in the instance
   */
  case class getStudents()

  /**
   * adds the professor for the session. If the professor is already participating in the session,
   * the professor is not added.
   * In either case, the ID of the professor is returned
   * @param professor (String) represents the professor name
   */
  case class addProfessor(professor: String)

  /**
   * removes the professor of ID professorID from the session.
   * This method keeps the consistency of the instance.
   * @param professorId (Integer) the identifier of the professor to remove
   */
  case class removeProfessor(professorId: Int)

  /**
   * updates the professor of identifier professorId (Integer) to the new name `professor` (String)
   * This method keeps the consistency of the instance.
   * @param professorId (Integer) the identifier of the professor to update
   * @param professor (String) represents the professor name
   */
  case class updateProfessor(professorId: Int, professor: String)

  /**
   * returns the name (String) of the professor of identifier professorId
   * @param professorId the identifier of the professor
   */
  case class getProfessorOfId(professorId: Int)

  /**
   * returns a set (strings) of all the professor names currently participating in the instance
   */
  case class getProfessors()

  /**
   * adds the course for the session. If the course is already participating in the session,
   * the course is not added.
   * In either case, the ID of the course is returned
   * @param course (String) represents the course name
   */
  case class addCourse(course: String)

  /**
   * removes the course of ID courseID from the session.
   * This method keeps the consistency of the instance.
   * @param courseId (Integer) the identifier of the course to remove
   */
  case class removeCourse(courseId: Int)

  /**
   * updates the course of identifier courseId (Integer) to the new name `course` (String)
   * This method keeps the consistency of the instance.
   * @param courseId (Integer) the identifier of the course to update
   * @param course (String) represents the course name
   */
  case class updateCourse(courseId: Int, course: String)

  /**
   * returns the name (String) of the course of identifier courseId
   * @param courseId the identifier of the course
   */
  case class getCourseOfId(courseId: Int)

  /**
   * returns a set (strings) of all the course names currently participating in the instance
   */
  case class getCourses()

  /**
   * adds the link 'follows' between the corresponding student and course
   * @param studentId (Integer) the identifier of the student
   * @param courseId (Integer) the identifier of the course
   */
  case class addStudentFollowingCourse(studentId: Int, courseId: Int)

  /**
   * removes the link 'follows' between the corresponding studend and course
   * @param studentId (Integer) the identifier of the student
   * @param courseId (Integer) the identifier of the course
   */
  case class removeStudentFollowingCourse(studentId: Int, courseId: Int)

  /**
   * returns the set (strings) of student names that follow the course
   * @param courseId (Integer) the identifier of the course
   */
  case class getStudentsFollowingCourse(courseId: Int)

  /**
   * returns the set (strings) of course names that are followed by the student
   * @param studentId (Integer) the identifier of the student
   */
  case class getCoursesFollowedByStudent(studentId: Int)

  /**
   * adds the link 'teaches' between the corresponding professor and course
   * @param profId (Integer) the identifier of the student
   * @param courseId (Integer) the identifier of the course
   */
  case class addProfessorGivingCourse(profId: Int, courseId: Int)

  /**
   * removes the link 'teaches' between the corresponding professor and course
   * @param profId (Integer) the identifier of the student
   * @param courseId (Integer) the identifier of the course
   */
  case class removeProfessorGivingCourse(profId: Int, courseId: Int)

  /**
   * returns the set (strings) of professor names that teach the course
   * @param courseId (Integer) the identifier of the course
   */
  case class getProfessorsTeachingCourse(courseId: Int)

  /**
   * returns the set (strings) of course names that are taught by the professor
   * @param profId (Integer) the identifier of the professor
   */
  case class getCoursesTeachedByProfessor(profId: Int)

  /**
   * adds a constraint of type constraintType described by the arguments in args. If the constraint type is:
   * <ul>
   *  <li>fixed: args matches courseName :: sessions where courseName is the name of the course fixed and sessions is
   *   a list of strings, each one representing a slot (|sessions| == 1 <=> course \in unislot)</li>
   *  <li>unavailable: args matches courseName :: sessions where courseName is the name of the course unavailable at `sessions` is
   *   a list of strings, each one representing a slot</li>
   *  <li>oral: args matches courseName :: nSlots :: nStudentPerSession :: Nil where courseName is the name of the oral course,
   *   nSlots, the number of slots needed for the course and nStudentPerSession, the maximum number of student per slot</li>
   *  <li>multislot: args matches courseName :: nSlots :: Nil where courseName is the name of the multislot course and
   *  nSlots, the number of slots needed for the course (nSlots <= self ? getNumberOfSessionPerDay())</li>
   *  <li>prof-unavailable demands the same arguments as unavailable but courseName is replaced by the profName</li>
   * </ul>
   * returns the ID of the newly created constraint.
   * @param constraintType the type of the constraint: either fixed, unavailable, oral, multislot or prof-unavailable
   * @param args the arguments structured as explained
   */
  case class addESSConstraint(constraintType: String, args: List[String])

  /**
   * removes the constraint of identifier cId
   * @param cId the identifier of the constraint.
   */
  case class removeESSConstraint(cId: Int)

  /**
   * launches the search (for maxTime milliseconds) on the current instance.
   * the onSolution callback will be executed each time a solution is found
   * @param onSolution the callback to execute each time a solution is found.
   *                   Must handle two parameters: the solution itself and its statistics
   * @param maxTime the maximum time of research (solving)
   */
  case class search(onSolution: (Map[String, Array[Int]], scala.collection.Seq[(Int, Int)]) => Unit, maxTime: Int)

  /**
   * logs the instance metrics as defined in solver.ProblemData
   */
  case object PrintInstanceMetrics

  /**
   * removes all the data from the maps of the instance
   * (calls .clear() on all structures.
   */
  case object ClearData

}