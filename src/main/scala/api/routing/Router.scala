package api.routing

import akka.actor._
import akka.event.Logging
import api._
import api.model.EncapsulatedModel
import spray.routing.HttpServiceActor

import scala.collection.mutable._

/**
 * Created by mcrochelet on 12/05/15.
 * The router handles the routing of requests in respect with the pathPrefix.
 * All defined URLs can be found by looking for the tag '// URL ::' in the comments.
 * @see api.routing.routes for more info.
 * @param instances the map of all instances. (instanceName -> ActorRef)
 */
case class Router(instances: Map[instanceName, ActorRef]) extends HttpServiceActor {
  implicit val system = context.system
  implicit val log = Logging(context.system, this)

  import routes._
  def receive = runRoute(
  // if the log level is debug, the request is logged
    logRequest("[LOG REQUEST]", Logging.DebugLevel) {
      // require the header X-instance-name to be present forall routes (except /instance route)
      headerValueByName("X-instance-name") {
        instanceName =>
          // lazy instantiate the actor of corresponding instance when (truly) accessed for the first time.
          implicit val instance = instances.getOrElseUpdate(instanceName,
            system.actorOf(Props[EncapsulatedModel], s"dataActor-of-$instanceName"))

          pathPrefix("instance") {
            instanceRouteWithToken(instance)
          }~
          pathPrefix("student") {
            studentRoute(instance)
          }~
          pathPrefix("course") {
            courseRoute(instance)
          }~
          pathPrefix("professor") {
            professorRoute(instance)
          }~
          pathPrefix("constraint") {
            constraintRoute(instance)
          }
      }~
      path("instance") {
        instanceRouteWithoutToken(instances)
      }
    }
  )
}
