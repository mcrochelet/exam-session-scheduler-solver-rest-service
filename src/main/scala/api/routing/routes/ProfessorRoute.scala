package api.routing.routes

import akka.actor.ActorRef
import akka.event.LoggingAdapter
import akka.pattern.ask
import api.OTWModels._
import api._
import api.model.EncapsulatedModelProtocol._
import spray.http.HttpHeaders.RawHeader
import spray.http.MediaTypes._
import spray.routing.Directives._
import spray.routing._

import scala.collection.mutable

/**
 * Created by mcrochelet on 12/05/15.
 * Handles all the endpoints beginning with /professor.
 */
object ProfessorRoute {

  def route(implicit actor: ActorRef, log: LoggingAdapter): Route =
    pathEnd {
      accessProfessors
    }~
    pathPrefix(IntNumber) { case pid =>

      onSuccess((actor ? getProfessorOfId(pid)).mapTo[String]) { case pName =>
        if (pName.size > 0)

          pathEnd {
            accessProfessor(pid, Professor(pName))
          }~
          pathPrefix("course") {
            pathEnd {
              accessCoursesOfProfessor(pid, Professor(pName))
            }~
            path(IntNumber) { case cid =>
              onSuccess((actor ? getCourseOfId(cid)).mapTo[String]) { case cName =>
                if (cName.size > 0) linkProfessorToCourse((pid, Professor(pName)), (cid, Course(cName)))
                else respondWithStatus(404) & complete {
                  s"course of id $cid does not exists for provided instance"
                }
              }
            }
          }

        else respondWithStatus(404) & complete { s"professor of id $pid does not exists for provided instance" }
      }
    }

  // URL :: /professor[PATHEND]
  def accessProfessors(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /professor")
        (actor ? getProfessors()).mapTo[mutable.Set[String]] map { professors =>
          professors.map { pName => Professor(pName)}.toSeq.toJson.compactPrint
        }
      }
    }~
    post {
      entity(as[Professor]) { case professor =>
        log.debug("handling POST :: /professor")
        onSuccess((actor ? addProfessor(professor.name)).mapTo[Int]) { case pid =>
          val pLocation = s"/professor/$pid"
          respondWithStatus(201) & respondWithHeader(RawHeader("Location", pLocation)) & complete {
            pLocation
          }
        }

      }
    }

  // URL :: /professor/IntNumber[PATHEND]
  def accessProfessor(pid: Int, professor: Professor)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /professor/IntNumber")
        professor
      }
    }~
    put {
      entity(as[Professor]) { case professor =>
        log.debug("handling PUT :: /professor/IntNumber")
        onSuccess((actor ? updateProfessor(pid, professor.name)).mapTo[Boolean]) {
          case true => respondWithStatus(204) & respondWithHeader(RawHeader("Location", s"/professor/$pid")) & complete {""}
          case false => respondWithStatus(505) & complete {"The server encountered an unexpected error"}
        }
      }
    }~
    delete {
      respondWithStatus(204) & complete {
        log.debug("handling DELETE :: /professor/IntNumber")
        actor ! removeProfessor(pid)
        ""
      }
    }

  // URL :: /professor/IntNumber/course[PATHEND]
  def accessCoursesOfProfessor(pid: Int, professor: Professor)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /professor/IntNumber/course")
        (actor ? getCoursesTeachedByProfessor(pid)).mapTo[mutable.Set[String]] map { courses =>
          courses.map { cName => Course(cName)}.toSeq.toJson.compactPrint
        }
      }
    }

  // URL :: /professor/IntNumber/course/IntNumber[PATHEND]
  def linkProfessorToCourse(s: (Int, Professor), c: (Int, Course))(implicit actor: ActorRef, log: LoggingAdapter): Route = {
    val (pid, professor) = s
    val (cid, course) = c
    post {
      onSuccess((actor ? addProfessorGivingCourse(pid, cid)).mapTo[Boolean]) {
        case true =>
          log.debug(s"handling POST :: /professor/$pid/course/$cid")
          val pLocation = s"/professor/$pid/course/$cid"
          respondWithStatus(201) & respondWithHeader(RawHeader("Location", pLocation)) & complete {
            s"Professor ${professor.name} now teaches ${course.name}"
          }
        case false => respondWithStatus(505) & complete {"The server encountered an unexpected error"}
      }
    }~
    delete {
      onSuccess((actor ? removeProfessorGivingCourse(pid, cid)).mapTo[Boolean]) {
        case true =>
          log.debug(s"handling DELETE :: /professor/$pid/course/$cid")
          respondWithStatus(204) & complete {""}
        case false => respondWithStatus(505) & complete {"The server encountered an unexpected error"}
      }
    }
  }
}
