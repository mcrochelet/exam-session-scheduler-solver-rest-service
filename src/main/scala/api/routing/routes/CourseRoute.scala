package api.routing.routes

import akka.actor.ActorRef
import akka.event.LoggingAdapter
import akka.pattern.ask
import api.OTWModels._
import api._
import api.model.EncapsulatedModelProtocol._
import spray.http.HttpHeaders.RawHeader
import spray.http.MediaTypes._
import spray.routing.Directives._
import spray.routing._

import scala.collection.mutable
/**
 * Created by mcrochelet on 12/05/15.
 * Handles all the endpoints beginning with /course.
 */
object CourseRoute {

  def route(implicit actor: ActorRef, log: LoggingAdapter): Route =
    pathEnd {
      accessCourses
    }~
    pathPrefix(IntNumber) { case cid =>

      onSuccess((actor ? getCourseOfId(cid)).mapTo[String]) { case cName =>
        if (cName.size > 0)

          pathEnd {
            accessCourse(cid, Course(cName))
          }~
          path("student") {
            accessStudentsOfCourse(cid, Course(cName))
          }~
          path("professor"){
            accessProfessorsOfCourse(cid, Course(cName))
          }

        else respondWithStatus(404) & complete { s"course of id $cid does not exists for provided instance" }
      }
    }

  // URL :: /course[PATHEND]
  def accessCourses(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /course")
        (actor ? getCourses()).mapTo[mutable.Set[String]] map { courses =>
          courses.map { cName => Course(cName)}.toSeq.toJson.compactPrint
        }
      }
    }~
    post {
      entity(as[Course]) { case course =>
        log.debug("handling POST :: /course")
        onSuccess((actor ? addCourse(course.name)).mapTo[Int]) { case cid =>
          val cLocation = s"/course/$cid"
          respondWithStatus(201) & respondWithHeader(RawHeader("Location", cLocation)) & complete {
            cLocation
          }
        }
      }
    }

  // URL :: /course/IntNumber[PATHEND]
  def accessCourse(cid: Int, course: Course)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /course/IntNumber")
        course
      }
    }~
    put {
      entity(as[Course]) { case course =>
        log.debug("handling PUT :: /course/IntNumber")
        onSuccess((actor ? updateCourse(cid, course.name)).mapTo[Boolean]) {
          case true => respondWithStatus(204) & respondWithHeader(RawHeader("Location", s"/course/$cid")) & complete {""}
          case false => respondWithStatus(505) & complete {"The server encountered an unexpected error"}
        }
      }
    }~
    delete {
      respondWithStatus(204) & complete {
        log.debug("handling DELETE :: /course/IntNumber")
        actor ! removeCourse(cid)
        ""
      }
    }

  // URL :: /course/IntNumber/student[PATHEND]
  def accessStudentsOfCourse(cid: Int, course: Course)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /course/IntNumber/student")
        (actor ? getStudentsFollowingCourse(cid)).mapTo[mutable.Set[String]] map { students =>
          students.map { sName => Student(sName)}.toSeq.toJson.compactPrint
        }
      }
    }

  // URL :: /course/IntNumber/professor[PATHEND]
  def accessProfessorsOfCourse(cid: Int, course: Course)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /course/IntNumber/student")
        (actor ? getProfessorsTeachingCourse(cid)).mapTo[mutable.Set[String]] map { professors =>
          professors.map { pName => Professor(pName)}.toSeq.toJson.compactPrint
        }
      }
    }
}
