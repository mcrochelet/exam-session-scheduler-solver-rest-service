package api.routing

import akka.actor.ActorRef
import akka.event.LoggingAdapter

import scala.collection.mutable

/**
 * @author mcrochelet
 * Implicit conversion from function to object
 */
package object routes {
  // only route that does not require the X-instance-name, the route is instantiated with the whole map
  def instanceRouteWithoutToken (instances: mutable.Map[String, ActorRef])(implicit log: LoggingAdapter) = InstanceRoute.routeWithoutToken(instances)

  // each route is instantiated with the corresponding actor (referenced by the X-instance-name)
  // and the logger (in order to be able to log consistently)
  def instanceRouteWithToken  (instance: ActorRef)(implicit log: LoggingAdapter) = InstanceRoute.routeWithToken(instance, log)
  def studentRoute            (instance: ActorRef)(implicit log: LoggingAdapter) = StudentRoute.route(instance, log)
  def courseRoute             (instance: ActorRef)(implicit log: LoggingAdapter) = CourseRoute.route(instance, log)
  def professorRoute          (instance: ActorRef)(implicit log: LoggingAdapter) = ProfessorRoute.route(instance, log)
  def constraintRoute         (instance: ActorRef)(implicit log: LoggingAdapter) = ConstraintRoute.route(instance, log)
}
