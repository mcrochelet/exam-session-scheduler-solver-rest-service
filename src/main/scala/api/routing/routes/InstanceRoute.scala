package api.routing.routes

import akka.actor.{ActorRef, PoisonPill}
import akka.event.LoggingAdapter
import akka.io.IO
import akka.pattern.ask
import api.OTWModels._
import api._
import api.model.EncapsulatedModelProtocol._
import spray.can.Http
import spray.http.HttpHeaders.RawHeader
import spray.routing.Directives._
import spray.routing._

import scala.collection.mutable.Map
import scala.concurrent.Future
import scala.util.Random
import spray.http._
import spray.client.pipelining._

/**
 * Created by mcrochelet on 12/05/15.
 * @author mcrochelet
 * Handles all the endpoints beginning with /instance.
 * The routes accessed with the token X-instance-name are directed to routeWithToken while those without are
 * directed to routeWithoutToken
 */
object InstanceRoute {

  /*          tokenAlphabet =      A to Z     ++:     a to z     ++:     0 to 9*/
  private val tokenAlphabet = ((0x41 to 0x5a) ++: (0x61 to 0x7a) ++: (0x30 to 0x39)).map(x => x.asInstanceOf[Char])
  private def generateToken: String = (for (i <- 0 until 32) yield Random.nextInt(tokenAlphabet.size))
    .map(tokenAlphabet) mkString
  private def getUniqueToken(instances: Map[instanceName, ActorRef]): String = {
    var trial = generateToken
    while (instances.keySet.contains(trial)) trial = generateToken
    trial
  }

  // URL :: /instance[PATHEND] without X-instance-name as header
  def routeWithoutToken(instances: Map[instanceName, ActorRef])(implicit log: LoggingAdapter) : Route =
    get {
      respondWithStatus(200) & complete {
        getUniqueToken(instances)
      }
    }~
    delete {
      entity(as[String]) { case instanceName =>
        onSuccess((instances(instanceName) ? isSearchLaunched()).mapTo[Boolean]) { case searchLaunched =>
          if (!searchLaunched){
            log.info(s"Destroying instance $instanceName")
            if (instances.contains(instanceName)){
              instances(instanceName) ! ClearData
              instances(instanceName) ! PoisonPill
              instances -= instanceName
              respondWithStatus(204) & complete {""}
            } else respondWithStatus(403) & complete {""}
          } else respondWithStatus(403) & complete {"A search is running currently on this instance"}
        }
      }
    }

  def routeWithToken(implicit actor: ActorRef, log: LoggingAdapter): Route =
    path("search") {
      post {
        onSuccess((actor ? getCallbackAddress()).mapTo[(String, Int, String)]) { case (address, port, relPath) =>
          if (address.equals("notset")) {
            respondWithStatus(403) & complete {"Please register a callback in order to receive informations"}
          } else launchSearch(address, port, relPath)
        }
      }
    }~
    path("callback") {
      (post | put) {
        entity(as[Callback]) { case callback =>
          registerCallback(callback)
        }
      }
    }~
    path("session") {
      accessSession
    }

  // URL :: /instance/session[PATHEND]
  def accessSession(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      onSuccess((actor ? getNumberOfDays()).mapTo[Int]) { case nDays =>
        onSuccess((actor ? getNumberOfSessionPerDay()).mapTo[Int]) { case nSessionPerDay =>
          onSuccess((actor ? getFirstDay()).mapTo[Int]) { case firstDay =>
            respondWithStatus(200) & complete {
              log.debug("Handling GET /instance/session")
              Session(nDays, nSessionPerDay, firstDay)
            }
          }
        }
      }
    }~
    post {
      entity(as[Session]) { case sess =>
        log.debug("Handling POST /instance/session")
        actor ! setNumberOfDays(sess.nDays)
        actor ! setNumberOfSessionPerDay(sess.nSessionPerDay)
        actor ! setFirstDay(sess.firstDay)
        respondWithStatus(204) & complete {""}
      }
    }

  // URL :: /instance/search[PATHEND]
  def launchSearch(cb: String, port: Int, relPath: String)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    onSuccess((actor ? isSearchLaunched()).mapTo[Boolean]) { case searchLaunched =>
      log.info("Handling POST /instance/search")
      if (searchLaunched) respondWithStatus(409) & complete {"A search is already en route"}
      else parameters ('searchTime ? 600000 ){ case searchTime =>
        respondWithStatus(202) & complete {
          log.info(s"with searchTime=$searchTime")

          val onSolution: (Map[String, Array[Int]], scala.collection.Seq[(Int, Int)]) => Unit =
            (solution: Map[String, Array[Int]], stat: scala.collection.Seq[(Int, Int)]) => {
              val pipeline: Future[SendReceive] =
                for (
                  Http.HostConnectorInfo(connector, _) <-
                  IO(Http) ? Http.HostConnectorSetup(cb, port=port)
                ) yield sendReceive(connector)
              pipeline.flatMap(_(Post(relPath, Answer(solution.toMap, stat))))
            }
          actor ! search(onSolution, searchTime)
          "The search has been launched"
        }
      }
    }


  // URL :: /instance/callback[PATHEND]
  def registerCallback(callback: Callback)(implicit actor: ActorRef, log: LoggingAdapter): Route = {
    (respondWithStatus(204) & respondWithHeader(RawHeader("Location", "/instance/callback"))) & complete {
      actor ! setCallbackAddress(callback.address, callback.port, callback.relPath)
      log.debug("handling POST|PUT /instance/callback")
      ""
    }
  }

}
