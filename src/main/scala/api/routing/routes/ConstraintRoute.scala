package api.routing.routes

import akka.actor.ActorRef
import akka.event.LoggingAdapter
import akka.pattern.ask
import api.OTWModels._
import api._
import api.model.EncapsulatedModelProtocol._
import spray.http.HttpHeaders.RawHeader
import spray.routing.Directives._
import spray.routing._

/**
 * Created by mcrochelet on 12/05/15.
 * Handles all the endpoints beginning with /constraint.
 */
object ConstraintRoute {

  def route(implicit actor: ActorRef, log: LoggingAdapter): Route =
    pathEnd {
      createConstraint
    }~
    path(IntNumber) { case cid =>
      removeConstraint(cid)
    }

  // URL :: /constraint[PATHEND]
  def createConstraint(implicit actor: ActorRef, log: LoggingAdapter): Route =
    post {
      entity(as[Constraint]) { case constraint =>
        log.debug("handling POST :: /constraint")
        onSuccess((actor ? addESSConstraint(constraint.Type, constraint.args)).mapTo[Int]) { case cid =>
          val cLocation = s"/constraint/$cid"
          respondWithStatus(201) & respondWithHeader(RawHeader("Location", cLocation)) & complete {
            cLocation
          }
        }
      }
    }

  // URL :: /constraint/IntNumber[PATHEND]
  def removeConstraint(cid: Int)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    delete {
      respondWithStatus(204) & complete {
        log.debug("handling DELETE :: /constraint/IntNumber")
        actor ! removeESSConstraint(cid)
        ""
      }
    }
}
