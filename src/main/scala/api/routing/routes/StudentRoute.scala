package api.routing.routes

import akka.actor.ActorRef
import akka.event.LoggingAdapter
import akka.pattern.ask
import api.OTWModels._
import api._
import api.model.EncapsulatedModelProtocol._
import spray.http.HttpHeaders.RawHeader
import spray.http.MediaTypes._
import spray.routing.Directives._
import spray.routing._

import scala.collection.mutable

/**
 * Created by mcrochelet on 12/05/15.
 * Handles all the endpoints beginning with /constraint.
 */
object StudentRoute {

  def route(implicit actor: ActorRef, log: LoggingAdapter): Route =
    pathEnd {
      accessStudents
    }~
    pathPrefix(IntNumber) { case sid =>

      onSuccess((actor ? getStudentOfId(sid)).mapTo[String]) { case sName =>
        if (sName.size > 0)

          pathEnd {
            accessStudent(sid, Student(sName))
          }~
          pathPrefix("course") {
            pathEnd {
              accessCoursesOfStudent(sid, Student(sName))
            }~
            path(IntNumber) { case cid =>
              onSuccess((actor ? getCourseOfId(cid)).mapTo[String]) { case cName =>
                if (cName.size > 0) linkStudentToCourse((sid, Student(sName)), (cid, Course(cName)))
                else respondWithStatus(404) & complete {
                  s"course of id $cid does not exists for provided instance"
                }
              }
            }
          }

        else respondWithStatus(404) & complete { s"student of id $sid does not exists for provided instance" }
      }
    }

  // URL :: /student[PATHEND]
  def accessStudents(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /student")
        (actor ? getStudents()).mapTo[mutable.Set[String]] map { students =>
          students.map { sName => Student(sName)}.toSeq.toJson.compactPrint
        }
      }
    }~
    post {
      entity(as[Student]) { case student =>
        log.debug("handling POST :: /student")
        onSuccess((actor ? addStudent(student.name)).mapTo[Int]) { case sid =>
          val sLocation = s"/student/$sid"
          respondWithStatus(201) & respondWithHeader(RawHeader("Location", sLocation)) & complete {
            sLocation
          }
        }

      }
    }

  // URL :: /student/IntNumber[PATHEND]
  def accessStudent(sid: Int, student: Student)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /student/IntNumber")
        student
      }
    }~
    put {
      entity(as[Student]) { case student =>
        log.debug("handling PUT :: /student/IntNumber")
        onSuccess((actor ? updateStudent(sid, student.name)).mapTo[Boolean]) {
          case true => respondWithStatus(204) & respondWithHeader(RawHeader("Location", s"/student/$sid")) & complete {""}
          case false => respondWithStatus(505) & complete {"The server encountered an unexpected error"}
        }
      }
    }~
    delete {
      respondWithStatus(204) & complete {
        log.debug("handling DELETE :: /student/IntNumber")
        actor ! removeStudent(sid)
        ""
      }
    }

  // URL :: /student/IntNumber/course[PATHEND]
  def accessCoursesOfStudent(sid: Int, student: Student)(implicit actor: ActorRef, log: LoggingAdapter): Route =
    get {
      respondWithStatus(200) & respondWithMediaType(`application/json`) & complete {
        log.debug("handling GET :: /student/IntNumber/course")
        (actor ? getCoursesFollowedByStudent(sid)).mapTo[mutable.Set[String]] map { courses =>
          courses.map { cName => Course(cName)}.toSeq.toJson.compactPrint
        }
      }
    }

  // URL :: /student/IntNumber/course/IntNumber[PATHEND]
  def linkStudentToCourse(s: (Int, Student), c: (Int, Course))(implicit actor: ActorRef, log: LoggingAdapter): Route = {
    val (sid, student) = s
    val (cid, course) = c
    post {
      onSuccess((actor ? addStudentFollowingCourse(sid, cid)).mapTo[Boolean]) {
        case true =>
          log.debug(s"handling POST :: /student/$sid/course/$cid")
          val sLocation = s"/student/$sid/course/$cid"
          respondWithStatus(201) & respondWithHeader(RawHeader("Location", sLocation)) & complete {
            s"Student ${student.name} now follows ${course.name}"
          }
        case false => respondWithStatus(505) & complete {"The server encountered an unexpected error"}
      }
    }~
    delete {
      onSuccess((actor ? removeStudentFollowingCourse(sid, cid)).mapTo[Boolean]) {
        case true =>
          log.debug(s"handling DELETE :: /student/$sid/course/$cid")
          respondWithStatus(204) & complete {""}
        case false => respondWithStatus(505) & complete {"The server encountered an unexpected error"}
      }
    }
  }
}
