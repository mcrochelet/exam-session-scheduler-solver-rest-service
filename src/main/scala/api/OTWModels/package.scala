package api

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

/**
 * @author mcrochelet
 * Over the Wire models: definition of the objects that will be marshalled and sent
 * in a request body.
 * Their formats are defined as implicit values.
 */
package object OTWModels extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val StudentFormat = jsonFormat1(Student)
  implicit val CallbackFormat = jsonFormat3(Callback)
  implicit val ConstraintFormat = jsonFormat2(Constraint)
  implicit val ProfessorFormat = jsonFormat1(Professor)
  implicit val CourseFormat = jsonFormat1(Course)
  implicit val SessionFormat = jsonFormat3(Session)
  implicit val AnswerFormat = jsonFormat2(Answer)

  case class Student(name: String)

  case class Course(name: String)

  case class Session(nDays: Int, nSessionPerDay: Int, firstDay: Int)

  case class Constraint(Type: String, args: List[String])

  case class Professor(name: String)

  case class Callback(address: String, port: Int, relPath: String)

  case class Answer(solution: Map[String, Array[Int]], stat: Seq[(Int, Int)])
}
