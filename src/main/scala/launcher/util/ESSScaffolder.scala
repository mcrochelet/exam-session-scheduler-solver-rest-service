package launcher.util

import java.io.IOException

import solver.ProblemData
import util.mapToMapSet

import scala.collection.mutable.Set
import scala.io.Source

/**
 * Parser used by the standalone launcher in order to inflate the files it is given as arguments.
 * @param numberOfDays the number of days of the session
 * @param beginningDay the first day of the session (0 = Monday)
 * @param sessionPerDay the number of sessions per day
 * @param courseFile the course_students file to be parsed
 * @param examTypeFile the examType File (Orals & Multislots) to be parsed
 * @param sessionRestrictionFile the sessionRestrictionFile (Fixed and Unavailable) to be parsed
 */
class ESSScaffolder(numberOfDays: Int, beginningDay: Int, sessionPerDay: Int, var courseFile: String,
                    examTypeFile: Option[String] = None,
                    sessionRestrictionFile: Option[String] = None) extends ProblemData{
      
  /**
   * internal state recording the fact that the courseFile has been already parsed
   */
  private var parsed = false 

  this.nDays = numberOfDays
  this.sessionsPerDay = sessionPerDay
  this.firstDay = beginningDay
  
  /**
   * Parse the course file scanning for format: </br>
   * Fac;CourseId;Prof1, Prof2;StudyYear;FacNumberOfStudent;studentName;OptionOfStudent;Status</br>
   */
  def readCourseFile {
    if (!parsed) {
      try {
        val lines = Source.fromFile(courseFile).getLines()
        if (!lines.hasNext) {
          sys.error("Empty course file")
          System.exit(-1)
        }

        while (lines.hasNext) {
          val currentLine = lines.next
  
          val row = currentLine.split(";")
  
            // unpack the row
          val fac = row(0).replaceAll(" ", "")
          val courseId = row(1).replaceAll(" ", "")
          
          val profIds: Set[String] = Set()++ (for (prof <- row(2).split(",")) yield prof.replaceAll(" ", ""))
          
          val studyYear = row(3).replaceAll(" ", "")
          val nFac = row(4).replaceAll(" ", "").toInt
          val studentId = row(5).replaceAll(" ", "")
          val option = row(6).replaceAll(" ", "")
          val status = row(7).replaceAll(" ", "")
          val inscribedToCourse = status.equals("I") || status.equals("S")
  
          // Model data
          if (inscribedToCourse){
            coursesFollowedBy += studentId -> courseId
            studentFollowingCourse += courseId -> studentId
            courses += courseId
            students += studentId
            professors ++= profIds
            professorsGivingCourse += courseId -> profIds

            for (profId <- profIds)
              courseGivenByProfessor += profId -> courseId
          }
        }
        parsed = true
      } catch {
        case e: IOException => {
          sys.error("IOException" + e)
          System.exit(-1)
        }
      }
    }
  }

  
  /**
   * Parse the session restriction file scanning for format: </br>
   * SESSION_RESTRICTIONS:</br>
   * CourseId\trestrictedSlot1 restrictedSlot2 ... restrictedSlotN</br>
   * ...</br>
   * SESSION_FIXED:</br>
   * CourseId\tfixedSlot</br>
   * ...</br>
   */
  def readSessionRestrictionFile(filename: String) {
    try {
      val lines = Source.fromFile(filename).getLines()
      if (!lines.hasNext)
        println("No restrictions found\nPursuing without.")

      /*
       * Mode :=
       *  0: Restriction of sessions (exam A cannot take place in session B)
       *  1: Fixed sessions (exam A must take place in session B) 
       */
      var mode = 0
      while (lines.hasNext) {
        val currentLine = lines.next 

        /* find out the mode we're in */
        if (currentLine.equals("SESSION_RESTRICTIONS:"))
          mode = 0
        else if (currentLine.equals("SESSION_FIXED:"))
          mode = 1
        else {
          var res = currentLine.split("\t")

          val examId = res(0)
          val splittedValue = res(1).split(" ").map(sess => sess.toInt)
          if (mode == 0)
            sessionUnavailableFor += examId -> splittedValue
          else if (mode == 1)
            sessionFixedFor += examId -> splittedValue
        }
      }

    } catch {
      case e: IOException => {
        sys.error("IOException" + e)
        System.exit(-1)
      }
    }
  }

  
 /**
   * Parse the exam type file scanning for format: </br>
   * MULTI_SLOTS:</br>
   * CourseId nSlots</br>
   * ...</br>
   * ORALS:</br>
   * CourseId nSlots</br>
   * ...</br>
   */
  def readExamTypeFile(filename: String) {
    try {
      val lines = Source.fromFile(filename).getLines()
      if (!lines.hasNext)
        println("No exam type restriction found\nPursuing without.")

      /*
       * Mode :=
       *  0: Multi Slots exams
       *  1: Oral Exams 
       */
        
      var mode = 0
      while (lines.hasNext) {
        val currentLine = lines.next 
        /* find out the mode we're in */
        if (currentLine.equals("MULTI_SLOTS:"))
          mode = 0
        else if (currentLine.equals("ORALS:"))
          mode = 1
        else {
          val res = currentLine.split("\t")
          val examId = res(0)
          val nSlots = res(1).toInt
          if (mode == 0)
            numberOfSlotsFor += examId -> nSlots
          else if (mode == 1){
            for (profId <- professorsGivingCourse(examId))
              oralsGivenByProfessor += profId -> examId
            numberOfSlotsForOral += examId -> nSlots
            nStudentPerOralSession += examId -> res(2).toInt
          }
        }
      }
    } catch {
      case e: IOException => {
        sys.error("IOException" + e)
        System.exit(-1)
      }
    }
  }
  
  /**
   * set the internal variable coursefile to the parameter and provoke the reading of the file
   */
  def readCourseFile(file: String) {
    courseFile = file
    resetInstanceVariables
    readCourseFile
  }

  if (!courseFile.equals("")) readCourseFile
  
  examTypeFile match {
    case Some(filename) => readExamTypeFile(filename)
    case None           => println("No exam type restriction found\nPursuing without.")
  }
  
  sessionRestrictionFile match {
    case Some(filename) => readSessionRestrictionFile(filename) 
    case None           => println("No restrictions found\nPursuing without.")
  }
  
  
  for ((profId, exams) <- courseGivenByProfessor) 
        writtenGivenByProfessor += profId -> exams.diff(oralsGivenByProfessor(profId))
}