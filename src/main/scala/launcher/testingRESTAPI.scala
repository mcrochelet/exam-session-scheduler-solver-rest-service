package launcher

import java.io.IOException

import akka.actor._
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import api.OTWModels._
import spray.can.Http
import spray.client.pipelining._
import spray.http._

import scala.collection.mutable._
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.Source

/**
 * Testing object that parses the instance of June 2015 and sends it to the REST interface and checks that a solution
 * is found.
 * @see receiver.scala for the logging of answers
 */
object testingRESTAPI extends App {


  implicit val system           = ActorSystem("ess-client")
  //implicit val ec               = system.dispatcher
  implicit val timeout          = Timeout(5.seconds)
  import ExecutionContext.Implicits.global

  val courseFile                = "resources/courses_students.csv"
  val sessionResctrictionFile   = "resources/session_restriction.txt"
  val examTypeFile              = "resources/multislot_oral_exams.txt"

  val courses = Map[String, Int]()
  val students = Map[String, Int]()
  val professors = Map[String, Int]()

  val basicPipeline: Future[SendReceive] =
    for (
      Http.HostConnectorInfo(connector, _) <-
      IO(Http) ? Http.HostConnectorSetup("localhost", port = 8080)
    ) yield sendReceive(connector)


  def readCourseFile (send: HttpRequest => Future[HttpResponse]) {
    try {
      val lines = Source.fromFile(courseFile).getLines()
      if (!lines.hasNext) {
        sys.error("Empty course file")
        System.exit(-1)
      }

      while (lines.hasNext) {
        val currentLine = lines.next

        val row = currentLine.split(";")

        // unpack the row
        val fac = row(0).replaceAll(" ", "")
        val courseTag = row(1).replaceAll(" ", "")

        val profIds: Set[String] = Set()++ (for (prof <- row(2).split(",")) yield prof.replaceAll(" ", ""))

        val studyYear = row(3).replaceAll(" ", "")
        val nFac = row(4).replaceAll(" ", "").toInt
        val studentName = row(5).replaceAll(" ", "")
        val option = row(6).replaceAll(" ", "")
        val status = row(7).replaceAll(" ", "")
        val inscribedToCourse = status.equals("I") || status.equals("S")

        // Model data
        if (inscribedToCourse) {
          val studentResponse = Await result(send(Post("/student", Student(studentName))), timeout.duration)
          for (hdr <- studentResponse.headers if hdr.is("location"))
            students += studentName -> hdr.value.split("/").last.toInt

          val courseResponse = Await result(send(Post("/course", Course(courseTag))), timeout.duration)
          for (hdr <- courseResponse.headers if hdr.is("location"))
            courses += courseTag -> hdr.value.split("/").last.toInt

          Await ready (send(Post(s"/student/${students(studentName)}/course/${courses(courseTag)}")), timeout.duration)

          for (p <- profIds) {
            val resp = Await result(send(Post("/professor", Professor(p))), timeout.duration)
            for (hdr <- resp.headers if hdr.is("location"))
              professors += p -> hdr.value.split("/").last.toInt

            Await ready (send(Post(s"/professor/${professors(p)}/course/${courses(courseTag)}")), timeout.duration)
          }
        }
      }

    } catch {
      case e: IOException => {
        sys.error("IOException" + e)
        System.exit(-1)
      }
    }
  }

  def readConstraintFile(send: HttpRequest => Future[HttpResponse], filename: String) {
    try {
      val lines = Source.fromFile(filename).getLines()
      if (!lines.hasNext) println("Empty constraint file, continuing without")

      var constraintType = ""
      while (lines.hasNext) {
        val currentLine = lines.next

        if (currentLine.equals("SESSION_RESTRICTIONS:"))  constraintType = "unavailable"
        else if (currentLine.equals("SESSION_FIXED:"))    constraintType = "fixed"
        else if (currentLine.equals("MULTI_SLOTS:"))      constraintType = "multislot"
        else if (currentLine.equals("ORALS:"))            constraintType = "oral"

        else Await result(send(Post("/constraint", Constraint(constraintType, currentLine.split("\t").flatMap(_.split(" ")).toList))), timeout.duration)
      }
    } catch {
      case e: IOException => {
        sys.error("IOException" + e)
        System.exit(-1)
      }
    }
  }

  basicPipeline.flatMap(_(Get("/instance"))).onSuccess {
    case response =>
      val instanceName = response.entity.asString
      println(s"instance name is $instanceName")
      val sender = for (sr <- basicPipeline) yield addHeader("X-instance-name", instanceName) ~> sr
      val RESTApi: HttpRequest => Future[HttpResponse] = (r: HttpRequest) => {sender.flatMap(_(r))}

      Await ready(RESTApi(Post("/instance/session", Session(21, 2, 0))), timeout.duration)

      readCourseFile(RESTApi)
      readConstraintFile(RESTApi, sessionResctrictionFile)
      readConstraintFile(RESTApi, examTypeFile)

      Await ready(RESTApi(Post("/instance/callback", Callback("localhost", 9876, "/"))), timeout.duration)
      Await ready(RESTApi(Post("/instance/search?searchTime=600000")), timeout.duration)
    // can be done and works but if sent to fast, will kill solver data
    // Await ready(basicPipeline.flatMap(_(Delete("/instance", instanceName))), timeout.duration)
  }
}
