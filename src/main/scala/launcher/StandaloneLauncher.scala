package launcher
import java.util.Calendar
import java.util.concurrent.TimeUnit

import launcher.util.ESSScaffolder
import solver._

import scala.collection.mutable._

/**
 * Main Class capable of solving the instance when provided the corresponding files:
 * as first argument, the course_students file represents the enrollments of the students and the teachings of the professors
 * as second (optional) argument, the examTypeFile represents the examType constraints namely MultiSlots and Orals.
 * as third (iff examTypeFile has been provided) represents the exam slots constraints namely Fixed and Unavailable.
 */
object StandaloneLauncher extends App {

  // Args verification
  if (args.size < 1) {
    println("Usage : java -jar course_studentsFile [examTypeFile [sessionRestrictionFile]] ")
    System.exit(-1)
  }

  /* Setting starting date */
  val d1 = Calendar.getInstance()
  d1.setTimeInMillis(0);
  d1.set(2015, Calendar.JUNE, 23)
  
  /* Setting ending date */
  val d2 = Calendar.getInstance();
  d2.setTimeInMillis(0)
  d2.set(2015, Calendar.JUNE, 1)
  
  /* Computing number of days */
  val diff = TimeUnit.MILLISECONDS.toDays(Math.abs(d1.getTimeInMillis - d2.getTimeInMillis)) + 1
  
  val nDays: Int = diff.toInt
  val sessPerDay: Int = 2
  
  /**
   * 0 = Monday
   * 1 = Tuesday
   * ...
   * 5 = Saturday
   * 6 = Sunday
   */
  val dayOfWeek = (d1.get(Calendar.DAY_OF_WEEK) + 5) % 7

  val rawData = new ESSScaffolder(nDays, dayOfWeek, sessPerDay, args(0), sessionRestrictionFile = Option(args(2)), examTypeFile = Option(args(1)))

  rawData.printMetrics
  val model = new ESSModel(rawData) with SearchLauncher // with BarplotDays
  model.startSearch(maxTime=600000, (solution: Map[String, Array[Int]], stat: scala.collection.Seq[(Int, Int)]) => {
  }, () => {})
  
  
}
