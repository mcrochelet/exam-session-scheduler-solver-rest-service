package launcher

import akka.io.IO
import akka.pattern.ask
import api.OTWModels._
import spray.can._
import spray.routing.HttpServiceActor

import akka.actor.{Props, ActorSystem}
import akka.event.Logging
import akka.util.Timeout

import scala.concurrent.duration.DurationInt

/**
 * @author mcrochelet
 * Simple receiver, can be launched with the testingREST main class in order to print the solutions (sent on this webhook)
 * This class defiens a Listening Actor on port 9876 whose sole purpose is the logging of answers.
 */
class Receiver extends HttpServiceActor {

  def receive = runRoute(
    logRequest("[LOG REQUEST]", Logging.InfoLevel) {
      entity(as[Answer]) { case ans =>
        complete("")
      }

    }
  )
}

object receivingServer extends App {

  implicit val system   = ActorSystem("ess-RESTServer-Listener")
  implicit val timeout  = Timeout(5.seconds)
  implicit val ec       = system.dispatcher

  val master = system.actorOf(Props(new Receiver()), "receiver")
  IO(Http) ? Http.Bind(master, interface = "localhost", port = 9876)
  system.eventStream.setLogLevel(Logging.InfoLevel)
}