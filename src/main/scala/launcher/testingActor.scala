package launcher

import java.io.IOException

import akka.actor.{Props, _}
import akka.pattern.ask
import akka.util.Timeout
import api.model.EncapsulatedModelProtocol._
import api.model._

import scala.collection.mutable
import scala.collection.mutable.Buffer
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import scala.io.Source

/**
 * Testing object that parses the instance of June 2015 and sends it to an encapsulatedModel actor and checks that a solution
 * is found.
 */
object testingActor extends App{
  val nDays      = 24
  val sessPerDay = 2


  val courseFile                = "resources/courses_students.csv"
  val sessionResctrictionFile   = "resources/session_restriction.txt"
  val examTypeFile              = "resources/multislot_oral_exams.txt"

  implicit val system           = ActorSystem("ess-server")
  implicit val timeout          = Timeout(5.seconds)
  implicit val ec               = system.dispatcher
  val actor                     = system.actorOf(Props[EncapsulatedModel], "Testing-Actor")

  val courses                   = mutable.Set[String]()
  val students                  = mutable.Set[String]()
  val profs                     = mutable.Set[String]()

  /**
   * Parse the course file scanning for format: </br>
   * Fac;CourseId;Prof1, Prof2;StudyYear;FacNumberOfStudent;studentName;OptionOfStudent;Status</br>
   */
  def readCourseFile {
    try {
      val lines = Source.fromFile(courseFile).getLines()
      if (!lines.hasNext) {
        sys.error("Empty course file")
        System.exit(-1)
      }
      val fs = Buffer[Future[Boolean]]()
      while (lines.hasNext) {
        val currentLine = lines.next

        val row = currentLine.split(";")

        val courseId = row(1).replaceAll(" ", "")

        val profIds: Set[String] = (for (prof <- row(2).split(",")) yield prof.replaceAll(" ", "")) toSet

        val studentId = row(5).replaceAll(" ", "")
        val status = row(7).replaceAll(" ", "")
        val inscribedToCourse = status.equals("I") || status.equals("S")

        // Model data
        if (inscribedToCourse) {
          students += studentId
          courses += courseId
          profs ++= profIds
          val sid  = Await result ((actor ? addStudent(studentId)).mapTo[Int], timeout.duration)
          val cid  = Await result ((actor ? addCourse(courseId)).mapTo[Int], timeout.duration)
          val pids = for (p <- profIds) yield Await result ((actor ? addProfessor(p)).mapTo[Int], timeout.duration)


          fs += (actor ? addStudentFollowingCourse(sid, cid)).mapTo[Boolean]
          fs ++= (for (pid <- pids) yield (actor ? addProfessorGivingCourse(pid, cid)).mapTo[Boolean])
        }
        for (f <- fs) Await result (f, timeout.duration)  // wait for completion before moving on
      }
    } catch {
      case e: IOException => {
        sys.error("IOException" + e)
        System.exit(-1)
      }
    }
  }

  /**
   * TODO
   */
  def readConstraintFile(filename: String) {
    try {
      val lines = Source.fromFile(filename).getLines()
      if (!lines.hasNext) println("Empty constraint file, continuing without")

      var constraintType = ""
      val fs = mutable.Set[Future[Int]]()
      while (lines.hasNext) {
        val currentLine = lines.next

        if (currentLine.equals("SESSION_RESTRICTIONS:"))  constraintType = "unavailable"
        else if (currentLine.equals("SESSION_FIXED:"))    constraintType = "fixed"
        else if (currentLine.equals("MULTI_SLOTS:"))      constraintType = "multislot"
        else if (currentLine.equals("ORALS:"))            constraintType = "oral"

        else fs += (actor ? addESSConstraint(constraintType, currentLine.split("\t").flatMap(_.split(" ")).toList)).mapTo[Int]
      }
      for (f <- fs) Await result (f, timeout.duration) // wait for completion before moving on
    } catch {
      case e: IOException => {
        sys.error("IOException" + e)
        System.exit(-1)
      }
    }
  }

  actor ! setNumberOfDays(nDays)
  actor ! setNumberOfSessionPerDay(sessPerDay)

  readCourseFile

  /*readConstraintFile(sessionRestrictionFile)
  readConstraintFile(examTypeFile)*/

  actor ! search(onSolution = (solution: mutable.Map[String, Array[Int]], stat: scala.collection.Seq[(Int, Int)]) => {},
    maxTime = 600000)
  //actor ! PrintInstanceMetrics

/*
  Thread.sleep(70000)

  (actor ? isSearchLaunched()).mapTo[Boolean] onSuccess{ case s =>
      println(s"search is: ${if(s) "launched" else "idle"}")
  }

  actor ! PoisonPill
*/

}
