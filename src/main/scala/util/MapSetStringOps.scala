package util

import scala.collection.mutable._

class MapSet[A, B](m: Map[A, Set[B]]) {
  
  def get(key: A) = m.getOrElse(key, Set[B]())
  
  def iterator = m.iterator
  
  def +=(kv: (A, B)) = {
    kv match {
      case (key, value) => {
        val newV = this.get(key)
        newV += value
        m += key -> newV
        m
      }
    }
  } 
  
  def -=(kv: (A, B)) = {
    kv match {
      case (key, value) => {
        val newV = this.get(key)
        newV -= value
        m += key -> newV
        m
      }
    }
  }
}