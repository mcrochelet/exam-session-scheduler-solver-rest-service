import scala.collection.mutable._
import scala.reflect.ClassTag
import reflect.runtime.universe.TypeTag

package object util {
  implicit def mapToMapSet[A, B](m: Map[A, Set[B]]) = new MapSet(m)
  implicit def MapWithContainsValue[A, B](m: Map[A, B]) = new MapWithContainsValue(m)
  implicit def MapWithKeysOf[A:ClassTag:TypeTag, B](m: Map[A, B]) = new MapWithKeysOf[A, B](m)

}  