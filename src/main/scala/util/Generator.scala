package util

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import scala.collection.mutable._
import solver._
import scala.util.Random

object MainGenerator extends App {
  new Generator(236,464,220,5,1.8,"test01.csv")
}
class Generator(nCourses: Int, nStudents: Int, nProfessors: Int, nEnrollementPerStudent: Int, nProfPerExam: Double, filename: String) {
  // Actual values of our data
  // 464 students
  // 236 courses
  //    - 215 unislots
  //    - 2 multislots
  //    - 19 orals
  // 220 profs
  // 1.8 exam per professor
  // 5 exams per student mean
  println("Beginning")
  
  val rand = Random

  val totInscription = nStudents * nEnrollementPerStudent
  val totTeaching = nCourses * nProfPerExam

  /**
   * **********************
   * Student enrolment
   * **********************
   */

  // stud -> {courses}
  val studCourses: Map[Int, Set[Int]] = Map()

  var studentCounter = 0
  // Each course have one student
  for (course <- 0 until nCourses) {
    studCourses += ((rand.nextInt(nStudents), course))
    studentCounter += 1
  }
  // Each student have one course
  for (student <- 0 until nStudents) {
    studCourses += ((student, rand.nextInt(nCourses)))
    studentCounter += 1
  }
  // Other inscription are purely random
  while (studentCounter < totInscription) {
    studCourses += ((rand.nextInt(nStudents), rand.nextInt(nCourses)))
    studentCounter += 1
  }

  /**
   * **********************
   * Professor of courses
   * **********************
   */

  // courses -> {professors}
  val profCourses: Map[Int, Set[Int]] = Map()

  var professorCounter = 0
  for (course <- 0 until nCourses) {
    if (course > nProfessors)
      profCourses += ((course, (rand.nextInt(nProfessors))))
    else
      profCourses += ((course, course))
    professorCounter += 1
  }
  while (professorCounter < totTeaching) {
    profCourses += ((rand.nextInt(nCourses), rand.nextInt(nProfessors)))
    professorCounter += 1
  }

  /**
   * **********************
   * Printing
   * **********************
   */

  
  val file = new File("resources/" + filename)
  val bw = new BufferedWriter(new FileWriter(file))
  bw.write("course;profs;student;inscribed\n")
  for (student <- 0 until nStudents){
    for (course <- studCourses(student)){
      bw.write(course+";"+profCourses(course).mkString(", ")+";" + student + ";I\n")
    }
      
  }
  bw.close()

}