package util

import scala.collection.mutable.Map
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.TypeTag

/**
 * Created by mcrochelet on 9/05/15.
 */
class MapWithContainsValue[A, B] (m: Map[A, B]) {

  /**
   * Checks if value v if contained within the values of the map at least once
   * @param v the value to find in the map
   * @return true if v belongs to m.values
   */
  def containsValue(v: B): Boolean = m.exists{case (key, value) => value.equals(v)}
}

class MapWithKeysOf[A:ClassTag:TypeTag, B] (m: Map[A, B]) {

  /**
   * Returns the array of keys having value v
   * @param v the value to be find
   * @return the array of keys having value v
   */
  def keysOf(v: B): Array[A] = {
    val t =  for ((key, value) <- m if (value.equals(v))) yield key
    t.toArray[A]
  }

}
