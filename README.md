# [LINGI2990] Exam Session Scheduler 

This project strives to solve the scheduling problem of the exam session for the EPLs masters scheduling problem
The solver takes into account :
* the written exams 
* the oral exams
* the exams taking more than one continuous session

It then tries to optimize the solution to maximize the comfort of the students. 

The project is linked to the https://bitbucket.org/mcrochelet/exam-session-scheduler-frontend-web-application github project co-developed in the context of the master's thesis or Martin Crochelet and Romain Vanwelde
# Dependencies: 
* [OsCAR](https://bitbucket.org/oscarlib/oscar/wiki/Home)
* [spray](http://spray.io)